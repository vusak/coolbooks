package com.jiran.coolbooks.common.collage;

import android.util.SparseArray;

import com.jiran.coolbooks.data.Collage;
import com.jiran.coolbooks.data.CollageRegion;

import java.util.ArrayList;
import java.util.List;

/**
 * Generator with hardcoded numbers of collagees.
 * <p/>
 * Date: 4/8/2014
 * Time: 4:20 PM
 *
 * @author MiG35
 */
public final class SimpleCollageGenerator implements CollageFactory {

    private static final int ITEMS_COUNT = 10;
    private static final int ITEMS_PAGE_COUNT = 8;
    private final SparseArray<Collage> mCollages = new SparseArray<>();
    private double border = 0.01d;
    private double half = 0.005d;

    @Override
    public Collage getCollage(final int number) {
        if (number < 0 || number >= 10) {
            throw new IllegalArgumentException("SimpleCollageGenerator can create collagees from 0 to 8 items only");
        }
        Collage collage = mCollages.get(number);
        if (null == collage) {
            collage = generateCollage(number);
            mCollages.put(number, collage);
        }
        return collage;
    }

    @Override
    public int getCollageCount() {
        return ITEMS_PAGE_COUNT;
    }

    @Override
    public void setCollageBorder(double border) {
        this.border = border;
        this.half = border / 2d;
    }

    @SuppressWarnings({"ValueOfIncrementOrDecrementUsed", "MagicNumber"})
    private Collage generateCollage(final int number) {

        final List<CollageRegion> collageRegions = new ArrayList<>();
        int regionId = 0;

        if (0 == number) {
            collageRegions.add(new CollageRegion(regionId, 0d + border, 0d + border, 1d - border, 1d - border));
        } else if (1 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 0.5d - (half), 1d - border));
            collageRegions.add(new CollageRegion(regionId, 0.5d + (half), 0d + border, 1d - border, 1d - border));
        } else if (2 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 1d - border, 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId, 0d + border, 0.5d + (half), 1d - border, 1d - border));
        } else if (3 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 1d - border, 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0.5d + (half), 0.5d - (half), 1d - border));
            collageRegions.add(new CollageRegion(regionId, 0.5d + (half), 0.5d + (half), 1d - border, 1d - border));
        } else if (4 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 0.5d - (half), 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId++, 0.5d + (half), 0d + border, 1d - border, 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId, 0d + border, 0.5d + (half), 1d - border, 1d - border));
        } else if (5 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 0.5d - (half), 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0.5d + (half), 0.5d - (half), 1d - border));
            collageRegions.add(new CollageRegion(regionId, 0.5d + (half), 0d + border, 1d - border, 1d - border));
        } else if (6 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 0.5d - (half), 1d - border));
            collageRegions.add(new CollageRegion(regionId++, 0.5d + (half), 0d + border, 1d - border, 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId, 0.5d + (half), 0.5d + (half), 1d - border, 1d - border));
        } else if (7 == number) {
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0d + border, 0.5d - (half), 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId++, 0d + border, 0.5d + (half), 0.5d - (half), 1d - border));
            collageRegions.add(new CollageRegion(regionId++, 0.5d + (half), 0d + border, 1d - border, 0.5d - (half)));
            collageRegions.add(new CollageRegion(regionId, 0.5d + (half), 0.5d + (half), 1d - border, 1d - border));
        } else if (8 == number) {
            collageRegions.add(new CollageRegion(regionId, 0d + border, 0.4d + border, 1d - border, 1d - border));
        } else if (9 == number) {
            collageRegions.add(new CollageRegion(regionId, 0d + border, 0d + border, 1d - border, 0.6d - border));
        }
        return new Collage(collageRegions);
    }
}