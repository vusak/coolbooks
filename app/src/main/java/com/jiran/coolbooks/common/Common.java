package com.jiran.coolbooks.common;

public class Common {
    public static final String TYPE = "a";
    public static final int VERSION = 1;
    // HEADER_**
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_NO_CACHE = "no-cache";
    public static final String HEADER_APPLICATION_VERSION = "APPLICATION_VERSION";
    public static final String HEADER_OS = "OS";
    public static final String HEADER_CONTENT_TYPE = "content-type";
    public static final String HEADER_MULTIPART_FORM_DATA = "multipart/form-data";
    public static final int TIMEOUT = 20000;

    // RESULT_CODE_**
    public static final int RESULT_CODE_ERROR_OK = 0;
    public static final int RESULT_CODE_ERROR_NO_RESPONSE = -1;
    public static final int RESULT_CODE_ERROR_UNKNOWN = -2;
    public static final int RESULT_CODE_ERROR_CANCELED = -3;
    public static final String PARAM_PROGRESS_MESSAGE = "progressMessage";
    public static final String PARAM_SHOW_ERROR_MESSAGE = "showErrorMessage";
    public static final String RESULT_CODE_ERROR_NO_DATA = "E01";
    public static final String RESULT_CODE_ERROR_VERSION = "E98";
    public static final String RESULT_CODE_ERROR_LOGIN_FAIL = "E99";

    public static final String PARAM_PASSWORD = "passwd";
    public static final String PARAM_USER_KEY = "person_objid";
    public static final String PARAM_CHANNEL_ID = "site_id";
    public static final String PARAM_BOOK_ID = "book_objid";
    public static final String PARAM_BOOK_COMMENT = "comment";
    public static final String PARAM_BOOK_COMMENT_ID = "comment_objid";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_VERSION = "version";

    public static final String TAG_RESULT = "result";
    public static final String TAG_MESSAGE = "message";

    public static final String TAG_VERSION = "version";

    public static final String ARG_RESULT = "result";
    public static final String ARG_SUCCESS = "success";
    public static final String ARG_LOGIN_RESULT = "login_result";
    public static final String ARG_COMMENT_LIST = "comment_list";
    public static final String ARG_CONNECT_POWER = "connect_power";
    public static final String ARG_DISCONNECT_POWER = "disconnect_power";

    public static final String ARG_START_TYPE = "start_type";
    public static final String ARG_START_TYPE_POWER_CONNECT = "type_power_connect";
    public static final String ARG_START_TYPE_POWER_DISCONNECT = "type_power_disconnect";
    public static final String ARG_START_TYPE_SCREEN_OFF = "type_screen_off";
    public static final String ARG_EPUB_URL = "epub_url";
    public static final String ARG_PDF_URL = "pdf_url";
    public static final String ARG_VIDEO_URL = "video_url";
    public static final String ARG_SLEEP_MODE = "sleep_mode";
    public static final String ARG_MULTI_MODE = "multi_mode";
    public static final String ARG_CURRENT_ACTIVITY = "current_activity";
    public static final String ARG_ERROR_MESSAGE = "error_message";

    public static final String ARG_VERSION = "version_check";

    public static final String URL_COOLBOOKS = "https://m.coolbooks.co.kr";
    public static final String URL_COOLBOOKS_SCHOOL = "https://m.coolbooks.co.kr/school";
    public static final String URL_COOLBOOKS_BOOKSHELF = "https://m.coolbooks.co.kr/bookshelf";
    public static final String URL_COOLBOOKS_PHOTOBOOK = "https://m.coolbooks.co.kr/photobook";

    public static final String URL_PRIVACY_POLICY = "http://www.coolschool.co.kr/contract/privacy?type=html";

    public static final String API_BASE_URL_TEST = "https://dev-coolbooksuser.coolschool.co.kr/epub-converter/api/v1/";
    public static final String API_BASE_URL = "https://coolbooksuser.coolschool.co.kr/epub-converter/api/v1/";

    public static final String API_PUBLISH_FILES = "publish/files";
    public static final int API_CODE_PUBLISH_FILES = 3000;

    public static final String PARAM_SESSION_ID = "session_id";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_SITE_ID = "site_id";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_AUTHOR = "author";
    public static final String PARAM_PUBLISHER = "publisher";
    public static final String PARAM_TEMPLATE_NAME = "template_name";
    public static final String PARAM_EBOOK_WIDTH = "ebook_width";
    public static final String PARAM_EBOOK_HEIGHT = "ebook_height";
    public static final String PARAM_COVER_WIDTH = "cover_width";
    public static final String PARAM_COVER_HEIGHT = "cover_height";
    public static final String PARAM_COVER_FILE = "cover_file";
    public static final String PARAM_PAGE_FILE_ = "page_file_";

    public static final String INTENT_PROJECT_ID = "project_id";
    public static final String INTENT_PAGE_ID = "page_id";
    public static final String INTENT_PAGE_INDEX = "page_index";
}
