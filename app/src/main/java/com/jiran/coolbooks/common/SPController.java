package com.jiran.coolbooks.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

public class SPController {
    public static final String LOGINED_USER_ID = "logined_user_id";
    public static final String DEFAULT_LOGINED_USER_ID = "";

    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getLoginedUserId() {
        return sp.getString(LOGINED_USER_ID, DEFAULT_LOGINED_USER_ID);
    }

    public void setLoginedUserId(String val) {
        put(LOGINED_USER_ID, val);
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}
