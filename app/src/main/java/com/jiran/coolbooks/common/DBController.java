package com.jiran.coolbooks.common;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiran.coolbooks.data.BrowserItem;
import com.jiran.coolbooks.data.CollageRegionData;
import com.jiran.coolbooks.data.PageItem;
import com.jiran.coolbooks.data.ProjectItem;

import java.util.List;

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "coolbooks.db";
    private static final int DB_VERSION = 1;
    private static DBHelper helper;
    private static SQLiteDatabase db;
    private Context context;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    /**
     * Browser Items
     */
    public synchronized BrowserItem getBrowserItem(String id) {
        return BrowserItem.getBrowserItem(db, id);
    }

    public synchronized List<BrowserItem> getBrowserItems() {
        return BrowserItem.getBrowserItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateBrowserItem(BrowserItem item) {
        return BrowserItem.insertOrUpdateBrowserItem(db, item);
    }

    public synchronized boolean updateBrowser(BrowserItem item) {
        return BrowserItem.updateBrowser(db, item);
    }

    public synchronized boolean deleteBrowserItem(BrowserItem item) {
        return BrowserItem.deleteBrowserItem(db, item);
    }

    public synchronized boolean deleteAllBrowserItems() {
        return BrowserItem.deleteAllBrowserItems(db);
    }

    /**
     * Project Item
     */
    public synchronized ProjectItem getProjectItem(String id) {
        return ProjectItem.getProjectItem(db, id);
    }

    public synchronized List<ProjectItem> getProjectItems() {
        return ProjectItem.getProjectItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateProjectItem(ProjectItem item) {
        return ProjectItem.insertOrUpdateProjectItem(db, item);
    }

    public synchronized boolean deleteProjectItem(ProjectItem item) {
        return ProjectItem.deleteProjectItem(db, item);
    }

    public synchronized boolean deleteAllProjectItems() {
        return ProjectItem.deleteAllProjectItems(db);
    }

    public synchronized void clearProject() {
        deleteAllPageItems();
        deleteAllCollageRegionData();
        deleteAllProjectItems();
    }

    /**
     * Page Item
     */
    public synchronized PageItem getPageItem(String id) {
        return PageItem.getPageItem(db, id);
    }

    public synchronized List<PageItem> getPageItemByProject(String projectId) {
        return PageItem.getPageItemByProject(db, projectId);
    }

    public synchronized List<PageItem> getPageItems() {
        return PageItem.getPageItems(db, null, null, null, null, null, PageItem.PAGE_INDEX + " ASC", null);
    }

    public synchronized boolean insertOrUpdatePageItem(PageItem item) {
        return PageItem.insertOrUpdatePageItem(db, item);
    }

    public synchronized boolean deletePageItem(PageItem item) {
        return PageItem.deletePageItem(db, item);
    }

    public synchronized boolean deletePageItemByProject(String projectId) {
        return PageItem.deletePageItemByProject(db, projectId);
    }

    public synchronized boolean deleteAllPageItems() {
        return PageItem.deleteAllPageItems(db);
    }

    /**
     * Collage Region Data
     */
    public synchronized CollageRegionData getCollageRegionData(int id, String pageId) {
        return CollageRegionData.getCollageRegionData(db, id, pageId);
    }

    public synchronized List<CollageRegionData> getCollageRegionDataByPage(String pageId) {
        return CollageRegionData.getCollageRegionDataByPage(db, pageId);
    }

    public synchronized List<CollageRegionData> getCollageRegionData() {
        return CollageRegionData.getCollageRegionData(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateCollageRegionData(CollageRegionData item) {
        return CollageRegionData.insertOrUpdateCollageRegionData(db, item);
    }

    public synchronized boolean deleteCollageRegionData(CollageRegionData item) {
        return CollageRegionData.deleteCollageRegionData(db, item);
    }

    public synchronized boolean deleteCollageRegionDataByPage(String pageId) {
        return CollageRegionData.deleteCollageRegionDataByPage(db, pageId);
    }

    public synchronized boolean deleteAllCollageRegionData() {
        return CollageRegionData.deleteAllCollageRegionData(db);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (oldVersion) {
            }
        }


        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            BrowserItem.createTableBrowserItems(db);
            CollageRegionData.createTableCollageRegionData(db);
            ProjectItem.createTableProject(db);
            PageItem.createTablePage(db);
        }
    }
}
