package com.jiran.coolbooks.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.CollageViewGroup;
import com.jiran.coolbooks.common.collage.CollageFactory;
import com.jiran.coolbooks.data.Collage;

/**
 * <p/>
 * Date: 4/8/2014
 * Time: 5:22 PM
 *
 * @author MiG35
 */
public class CollageAdapter extends BaseAdapter {

    private final CollageFactory mCollageFactory;
    private final LayoutInflater mLayoutInflater;

    private Integer mSelectedNumber;


    /**
     * Will show collage template items. Will use factory as collage generator.
     *
     * @param context        application or activity context
     * @param collageFactory factory for collage creation
     * @param selectedNumber previously selected collage. may be null if nothing selected.
     */
    public CollageAdapter(final Context context, final CollageFactory collageFactory, final Integer selectedNumber) {
        mLayoutInflater = LayoutInflater.from(context);
        mCollageFactory = collageFactory;
        mSelectedNumber = selectedNumber;
    }

    @Override
    public int getCount() {
        return mCollageFactory.getCollageCount();
    }

    @Override
    public Collage getItem(final int position) {
        return mCollageFactory.getCollage(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View resultView;
        final Holder holder;
        if (null == convertView) {
            resultView = mLayoutInflater.inflate(R.layout.item_collage, parent, false);
            holder = new Holder(resultView);
            resultView.setTag(R.id.holder, holder);
        } else {
            resultView = convertView;
            holder = (Holder) convertView.getTag(R.id.holder);
        }

//        holder.mCollageViewGroup.setCollage(getItem(position));
        holder.mCollageViewGroup.setVisibility(View.GONE);
//        holder.mCollage.setImageResource(collages[position]);
        if (null == mSelectedNumber || mSelectedNumber != position) {
            holder.mCheckBox.setVisibility(View.GONE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }

        return resultView;
    }

    public void setSelected(final int position) {
        mSelectedNumber = position;
        notifyDataSetChanged();
    }

    private static class Holder {

        final ImageView mCheckBox, mCollage;
        final CollageViewGroup mCollageViewGroup;

        Holder(final View resultView) {
            mCheckBox = (ImageView) resultView.findViewById(R.id.iv_selection_box);
            mCollage = resultView.findViewById(R.id.iv_collage);
            mCollageViewGroup = (CollageViewGroup) resultView.findViewById(R.id.collage_view_group);
        }
    }
}