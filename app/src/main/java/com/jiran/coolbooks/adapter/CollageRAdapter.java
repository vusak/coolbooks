package com.jiran.coolbooks.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.CollageViewGroup;
import com.jiran.coolbooks.data.Collage;

import java.util.ArrayList;

public class CollageRAdapter extends ReSelectableAdapter<Collage, CollageRAdapter.CollageHolder> {

    private Context context;
    private ArrayList<Collage> items;
    private int selectedIndex = 0;

    private int[] collages = {
            R.drawable.ic_layout_a_56,
            R.drawable.ic_layout_b_56,
            R.drawable.ic_layout_c_56,
            R.drawable.ic_layout_d_56,
            R.drawable.ic_layout_e_56,
            R.drawable.ic_layout_f_56,
            R.drawable.ic_layout_g_56,
            R.drawable.ic_layout_h_56
    };

    public CollageRAdapter(Context context, int layoutId, ArrayList<Collage> items, CollageRAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(CollageHolder holder, int position) {

        Collage item = items.get(position);
        holder.collageImage.setImageResource(collages[position]);
        holder.collageViewGroup.setVisibility(View.GONE);
//        holder.collageViewGroup.setCollage(item);
        holder.checkFrame.setVisibility(selectedIndex != position ? View.GONE : View.VISIBLE);
    }

    @Override
    public CollageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CollageHolder holder = new CollageHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.checkFrame = holder.fv(R.id.iv_selection_box);
        holder.collageViewGroup = holder.fv(R.id.collage_view_group);
        holder.collageImage = holder.fv(R.id.iv_collage);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public int getSelectedIndex() {
        return this.selectedIndex;
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public static interface CollageRAdapterListener extends ReOnItemClickListener<Collage> {
//        public void onInfoButtonClicked(int position, TeamItem item);
//
//        public void onDeleteButtonClick(int positon, TeamItem item);
    }

    public class CollageHolder extends ReAbstractViewHolder {
        FrameLayout root;
        ImageView checkFrame, collageImage;
        CollageViewGroup collageViewGroup;
//        private OnItemViewClickListener settingButtonClickListener, deleteButtonClickListener;

        public CollageHolder(View itemView) {
            super(itemView);
        }

//        public void setOnInfoButtonClickListener(OnItemViewClickListener listener) {
//            settingButtonClickListener = listener;
//            if (listener != null && infoButton != null) {
//                infoButton.setOnClickListener(v -> settingButtonClickListener.onItemViewClick(getPosition(), AppHolder.this));
//            }
//        }
//
//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && deleteButton != null) {
//                deleteButton.setOnClickListener(v ->deleteButtonClickListener.onItemViewClick(getAdapterPosition(), AppHolder.this));
//            }
//        }
    }
}
