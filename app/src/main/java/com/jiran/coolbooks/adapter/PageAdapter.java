package com.jiran.coolbooks.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.data.PageItem;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class PageAdapter extends ReSelectableAdapter<PageItem, PageAdapter.PageHolder> {

    boolean clearCache = false;
    private Context context;
    private ArrayList<PageItem> items;
    private int selectedIndex = 0;
    private boolean deleteMode = false;

    public PageAdapter(Context context, int layoutId, ArrayList<PageItem> items, PageAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(PageHolder holder, int position) {

        PageItem item = items.get(position);
        if (!TextUtils.isEmpty(item.getPageFileUrl())) {
            if (clearCache)
                Picasso.with(context).load(new File(item.getPageFileUrl())).skipMemoryCache().error(R.drawable.ic_none_image_56).into(holder.thumbnail);
            else
                Picasso.with(context).load(new File(item.getPageFileUrl())).error(R.drawable.ic_none_image_56).into(holder.thumbnail);
//        if (item.getThumbnail() != null) {
//            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getThumbnail()));
        } else
            holder.thumbnail.setImageResource(R.drawable.ic_none_image_56);
        holder.checkFrame.setVisibility(selectedIndex == position && !deleteMode ? View.VISIBLE : View.GONE);
        holder.deleteButton.setVisibility(deleteMode && position != 0 ? View.VISIBLE : View.GONE);
        holder.moveButton.setVisibility(deleteMode && position != 0 ? View.VISIBLE : View.GONE);
        holder.pageText.setTextColor(ContextCompat.getColor(context, selectedIndex == position ? R.color.blue_a100 : R.color.gray4_a100));
        holder.pageText.setText(position == 0 ? context.getString(R.string.cover) : String.valueOf(item.getIndex()));
    }

    @Override
    public PageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PageHolder holder = new PageHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.checkFrame = holder.fv(R.id.image_selected);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.deleteButton = holder.fv(R.id.button_delete);
        holder.moveButton = holder.fv(R.id.button_move);
        holder.pageText = holder.fv(R.id.text_page);

        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    ((PageAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                setDeleteMode(true);
                if (listener != null && items.size() > position) {
                    listener.OnItemLongClick(position, items.get(position));
                }
                return true;
            }
        });

        holder.moveButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isDeleteMode() && event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (listener != null)
                        ((PageAdapterListener) listener).onStartDrag(holder);
                }
                return false;
            }
        });
        return holder;
    }

    public int getSelectedIndex() {
        return this.selectedIndex;
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean delete) {
        this.deleteMode = delete;
        notifyDataSetChanged();
    }

    public void refreshItems() {
        clearCache = true;
        notifyDataSetChanged();
        clearCache = false;
    }

    public static interface PageAdapterListener extends ReOnItemClickListener<PageItem> {
        public void onDeleteButtonClick(int positon, PageItem item);

        public void onStartDrag(ReAbstractViewHolder holder);
    }

    public class PageHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView checkFrame, thumbnail;
        ImageButton deleteButton, moveButton;
        TextView pageText;
        private OnItemViewClickListener deleteButtonClickListener;

        public PageHolder(View itemView) {
            super(itemView);
        }

        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
            deleteButtonClickListener = listener;
            if (listener != null && deleteButton != null) {
                deleteButton.setOnClickListener(v -> deleteButtonClickListener.onItemViewClick(getAdapterPosition(), PageHolder.this));
            }
        }
    }
}
