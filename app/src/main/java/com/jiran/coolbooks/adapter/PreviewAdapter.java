package com.jiran.coolbooks.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.data.PageItem;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class PreviewAdapter extends PagerAdapter {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<PageItem> items;

    public PreviewAdapter(Context context, ArrayList<PageItem> pages) {
        this.context = context;
        this.items = pages;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_preview, container, false);
        ImageView preview = (ImageView) v.findViewById(R.id.image_preview);
        TextView title = (TextView) v.findViewById(R.id.text_preview);
        title.setVisibility(View.GONE);
//        if (items.get(position).getThumbnail() != null)
//            preview.setImageBitmap(ImageUtil.getImage(items.get(position).getThumbnail()));
        if (!TextUtils.isEmpty(items.get(position).getPageFileUrl())) {
            Picasso.with(context).load(new File(items.get(position).getPageFileUrl())).skipMemoryCache().error(R.drawable.ic_none_image_56).into(preview);
            preview.setBackgroundResource(R.color.transparent);
        } else {
            preview.setBackgroundResource(R.color.gray4_a100);
            preview.setImageResource(R.drawable.ic_none_image_56);
        }

        title.setText(position == 0 ? context.getString(R.string.cover) : items.get(position).getIndex() + " " + context.getString(R.string.page));

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.invalidate();
    }
}