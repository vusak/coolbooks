package com.jiran.coolbooks;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.jiran.coolbooks.common.DBController;
import com.jiran.coolbooks.common.SPController;
import com.jiran.coolbooks.common.Util;
import com.jiran.coolbooks.network.JNetworkMonitor;
import com.squareup.picasso.Picasso;

public class BaseApplication extends Application {
    private SPController sp;
    private DBController db;
    private ResourceWrapper r;
    private JNetworkMonitor networkMonitor;

    private String version;
    private String os = "Android " + Build.VERSION.RELEASE;
    private String deviceName;
    private String locale;
    private String modelName = Build.MODEL;
    private Toast mOldToast;
    private Handler mHandler;

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();

        sp = new SPController(getApplicationContext());
        db = new DBController(getApplicationContext());
        r = new ResourceWrapper(getResources());

        networkMonitor = new JNetworkMonitor(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkMonitor, filter);

        version = Util.getVersion(getApplicationContext());
        deviceName = Build.MODEL;

        mHandler = new Handler();

        final Picasso picasso = Picasso.with(this);
        picasso.setLoggingEnabled(false);
    }

    public void onNetworkChanged(int status) {
        if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE) {
            Toast.makeText(this, "NETWORK_TYPE_MOBILE", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_WIFI) {
            Toast.makeText(this, "NETWORK_TYPE_WIFI", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            Toast.makeText(this, "NETWORK_TYPE_NOT_CONNECTED", Toast.LENGTH_SHORT).show();
        }
    }

    public SPController getSPController() {
        return this.sp;
    }

    public DBController getDBController() {
        return this.db;
    }

    public ResourceWrapper getResourceWrapper() {
        return r;
    }

    public JNetworkMonitor getNetworkMonitor() {
        return networkMonitor;
    }

    public String getOS() {
        return this.os;
    }

    public String getOSName() {
        return "Android";
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getVersion() {
        return this.version;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public String getLocale() {
        return this.locale;
    }

    public void showToast(final String message, final Integer gravity) {
        if (getMainLooper() == Looper.myLooper()) {
            if (null != mOldToast) {
                mOldToast.cancel();
            }
            mOldToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            if (null != gravity) {
                mOldToast.setGravity(gravity, 0, 0);
            }
            mOldToast.show();
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    showToast(message, gravity);
                }
            });
        }
    }

    public static class ResourceWrapper {
        private Resources r;

        public ResourceWrapper(Resources r) {
            this.r = r;
        }

        public int px(int id) {
            return r.getDimensionPixelSize(id);
        }

        public String s(int id) {
            return r.getString(id);
        }

        public String[] sa(int id) {
            return r.getStringArray(id);
        }

        public int c(Context context, int id) {
            if (context == null)
                return 0;
            else
                return ContextCompat.getColor(context, id);
        }

        public Drawable d(Context context, int id) {
            if (context == null)
                return null;
            else
                return ContextCompat.getDrawable(context, id);
        }

        public boolean b(int id) {
            return r.getBoolean(id);
        }

        public int i(int id) {
            return r.getInteger(id);
        }

        public long l(int id) {
            return Long.parseLong(s(id));
        }
    }
}
