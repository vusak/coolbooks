package com.jiran.coolbooks.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import com.jiran.coolbooks.base.BaseComparator;
import com.jiran.coolbooks.common.LogUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectItem extends TableObject {
    public static final String TABLE_NAME = "project";

    public static final String ROW_ID = "_id";
    public static final String PROJECT_ID = "project_id";
    public static final String PROJECT_USER_ID = "project_user_id";
    public static final String PROJECT_TITLE = "project_title";
    public static final String PROJECT_AUTHOR = "project_author";
    public static final String PROJECT_PUBLISHER = "project_publisher";
    public static final String PROJECT_COVER = "project_cover";
    public static final String PROJECT_CREATE_DATE = "project_create_date";
    public static final String PROJECT_EDIT_DATE = "project_edit_date";

    public static final Creator<ProjectItem> CREATOR = new Creator<ProjectItem>() {
        @Override
        public ProjectItem createFromParcel(Parcel source) {
            return new ProjectItem(source);
        }

        @Override
        public ProjectItem[] newArray(int size) {
            return new ProjectItem[size];
        }
    };

    private long id;
    private String projectId;
    private String userId;
    private String title;
    private String author;
    private String publisher;
    private byte[] cover;
    private long createDate;
    private long editDate;

    public ProjectItem() {
        super();
    }

    public ProjectItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.projectId = in.readString();
        this.userId = in.readString();
        this.title = in.readString();
        this.author = in.readString();
        this.publisher = in.readString();
        in.readByteArray(this.cover);
        this.createDate = in.readLong();
        this.editDate = in.readLong();
    }

    public static void createTableProject(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(PROJECT_ID + " text not null unique, ")
                .append(PROJECT_USER_ID + " text, ")
                .append(PROJECT_TITLE + " text, ")
                .append(PROJECT_AUTHOR + " text, ")
                .append(PROJECT_PUBLISHER + " text, ")
                .append(PROJECT_COVER + " BLOB, ")
                .append(PROJECT_CREATE_DATE + " integer, ")
                .append(PROJECT_EDIT_DATE + " integer) ")
                .toString());
    }

    public static ProjectItem getProjectItem(SQLiteDatabase db, String id) {
        List<ProjectItem> list = getProjectItems(db, null, PROJECT_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ProjectItem> getProjectItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ProjectItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ProjectItem item = new ProjectItem()
                        .setRowId(c)
                        .setProjectId(c)
                        .setUserId(c)
                        .setTitle(c)
                        .setAuthor(c)
                        .setPublisher(c)
                        .setCover(c)
                        .setCreateDate(c)
                        .setEditDate(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateProjectItem(SQLiteDatabase db, ProjectItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putProjectId(v);
            item.putUserId(v);
            item.putTitle(v);
            item.putAuthor(v);
            item.putPublisher(v);
            item.putCover(v);
            item.putCreateDate(v);
            item.putEditDate(v);

            int rowAffected = db.update(TABLE_NAME, v, PROJECT_ID + " =? ", new String[]{item.getProjectId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteProjectItem(SQLiteDatabase db, ProjectItem item) {
        try {
            db.delete(TABLE_NAME, PROJECT_ID + " = ? ", new String[]{item.getProjectId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteProjectItems(SQLiteDatabase db, ArrayList<ProjectItem> list) {
        for (ProjectItem item : list)
            deleteProjectItem(db, item);
        return false;
    }

    public static boolean deleteAllProjectItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(projectId);
        out.writeString(userId);
        out.writeString(title);
        out.writeString(author);
        out.writeString(publisher);
        out.writeByteArray(cover);
        out.writeLong(createDate);
        out.writeLong(editDate);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ProjectItem && projectId == ((ProjectItem) obj).projectId;
    }

    public long getRowId() {
        return this.id;
    }

    public ProjectItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ProjectItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ProjectItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getProjectId() {
        return this.projectId;
    }

    public ProjectItem setProjectId(Cursor c) {
        this.projectId = s(c, PROJECT_ID);

        return this;
    }

    public ProjectItem setProjectId(String id) {
        this.projectId = id;
        return this;
    }

    public ProjectItem putProjectId(ContentValues v) {
        v.put(PROJECT_ID, this.projectId);

        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public ProjectItem setUserId(Cursor c) {
        this.userId = s(c, PROJECT_USER_ID);

        return this;
    }

    public ProjectItem setUserId(String id) {
        this.userId = id;

        return this;
    }

    public ProjectItem putUserId(ContentValues v) {
        v.put(PROJECT_USER_ID, this.userId);

        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public ProjectItem setTitle(Cursor c) {
        this.title = s(c, PROJECT_TITLE);

        return this;
    }

    public ProjectItem setTitle(String title) {
        this.title = title;

        return this;
    }

    public ProjectItem putTitle(ContentValues v) {
        v.put(PROJECT_TITLE, this.title);

        return this;
    }

    public String getAuthor() {
        return this.author;
    }

    public ProjectItem setAuthor(Cursor c) {
        this.author = s(c, PROJECT_AUTHOR);

        return this;
    }

    public ProjectItem setAuthor(String author) {
        this.author = author;

        return this;
    }

    public ProjectItem putAuthor(ContentValues v) {
        v.put(PROJECT_AUTHOR, this.author);

        return this;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public ProjectItem setPublisher(Cursor c) {
        this.publisher = s(c, PROJECT_PUBLISHER);

        return this;
    }

    public ProjectItem setPublisher(String publisher) {
        this.publisher = publisher;

        return this;
    }

    public ProjectItem putPublisher(ContentValues v) {
        v.put(PROJECT_PUBLISHER, this.publisher);

        return this;
    }

    public byte[] getCover() {
        return cover;
    }

    public ProjectItem setCover(Cursor c) {
        this.cover = blob(c, PROJECT_COVER);

        return this;
    }

    public ProjectItem setCover(byte[] cover) {
        this.cover = cover;

        return this;
    }

    public ProjectItem putCover(ContentValues v) {
        v.put(PROJECT_COVER, this.cover);

        return this;
    }

    public long getCreateDate() {
        return this.createDate;
    }

    public ProjectItem setCreateDate(Cursor c) {
        this.createDate = l(c, PROJECT_CREATE_DATE);

        return this;
    }

    public ProjectItem setCreateDate(long createDate) {
        this.createDate = createDate;

        return this;
    }

    public ProjectItem putCreateDate(ContentValues v) {
        v.put(PROJECT_CREATE_DATE, this.createDate);

        return this;
    }

    public long getEditDate() {
        return this.editDate;
    }

    public ProjectItem setEditDate(Cursor c) {
        this.editDate = l(c, PROJECT_EDIT_DATE);

        return this;
    }

    public ProjectItem setEditDate(long editDate) {
        this.editDate = editDate;

        return this;
    }

    public ProjectItem putEditDate(ContentValues v) {
        v.put(PROJECT_EDIT_DATE, this.editDate);

        return this;
    }

    private static class PageComparator extends BaseComparator implements Comparator<ProjectItem> {
        public PageComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(ProjectItem lhs, ProjectItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }

        protected final int compareId(ProjectItem lhs, ProjectItem rhs) {
            return compareLong(lhs.getRowId(), rhs.getRowId(), false, true);
        }
    }

    private static class PageEditDateComparator extends PageComparator {
        public PageEditDateComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(ProjectItem lhs, ProjectItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareLong(lhs.getEditDate(), rhs.getEditDate(), true);
            if (result != 0)
                return result;
            else
                return compareString(lhs.getProjectId(), rhs.getProjectId(), false);
        }
    }
}
