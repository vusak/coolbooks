package com.jiran.coolbooks.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import com.jiran.coolbooks.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class CollageRegionData extends TableObject {
    public static final String TABLE_NAME = "region_data";
//    private static final long serialVersionUID = -7032708611705926844L;

    public static final String ROW_ID = "_id";
    public static final String REGION_ID = "region_id";
    public static final String REGION_PAGE_ID = "region_page_id";
    public static final String REGION_IMAGE_LEFT = "region_image_left";
    public static final String REGION_IMAGE_TOP = "region_image_top";
    public static final String REGION_IMAGE_SCALE = "region_image_scale";
    public static final String REGION_IMAGE_URI = "region_image_uri";
    public static final Creator<CollageRegionData> CREATOR = new Creator<CollageRegionData>() {
        @Override
        public CollageRegionData createFromParcel(Parcel source) {
            return new CollageRegionData(source);
        }

        @Override
        public CollageRegionData[] newArray(int size) {
            return new CollageRegionData[size];
        }
    };
    private long id;
    private int regionId;
    private String pageId;
    private Float imageLeft;
    private Float imageTop;
    private float imageScale;
    private String imageUrl;

    public CollageRegionData() {
        super();
    }

    public CollageRegionData(final String imageFile) {
        if (null == imageFile) {
            throw new IllegalArgumentException("imageFile can't be null");
        }
//        this.imageFile = imageFile;
        this.imageUrl = imageFile;
        imageScale = 1f;
    }

    public CollageRegionData(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.regionId = in.readInt();
        this.pageId = in.readString();
        this.imageLeft = in.readFloat();
        this.imageTop = in.readFloat();
        this.imageScale = in.readFloat();
        this.imageUrl = in.readString();
    }

    public static void createTableCollageRegionData(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(REGION_ID + " integer, ")
                .append(REGION_PAGE_ID + " text, ")
                .append(REGION_IMAGE_LEFT + " real, ")
                .append(REGION_IMAGE_TOP + " real, ")
                .append(REGION_IMAGE_SCALE + " real, ")
                .append(REGION_IMAGE_URI + " text not null) ")
                .toString());
    }

    public static CollageRegionData getCollageRegionData(SQLiteDatabase db, int id, String pageId) {
        List<CollageRegionData> list = getCollageRegionData(db, null, REGION_ID + " =? AND " + REGION_PAGE_ID + " =? ", new String[]{"" + id, pageId}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<CollageRegionData> getCollageRegionDataByPage(SQLiteDatabase db, String pageId) {
        List<CollageRegionData> list = getCollageRegionData(db, null, REGION_PAGE_ID + " = ? ", new String[]{pageId}, null, null, null, null);

        return list;
    }

    public static List<CollageRegionData> getCollageRegionData(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<CollageRegionData> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                CollageRegionData item = new CollageRegionData()
                        .setRowId(c)
                        .setRegionId(c)
                        .setRegionPageId(c)
                        .setImageLeft(c)
                        .setImageTop(c)
                        .setImageScale(c)
                        .setRegionImageUrl(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateCollageRegionData(SQLiteDatabase db, CollageRegionData item) {
        try {
            ContentValues v = new ContentValues();
            item.putRegionId(v)
                    .putRegionPageId(v)
                    .putImageLeft(v)
                    .putImageTop(v)
                    .putImageScale(v)
                    .putRegionImageUrl(v);

            int rowAffected = db.update(TABLE_NAME, v, REGION_ID + " =? AND " + REGION_PAGE_ID + " =? ", new String[]{"" + item.getRegionId(), item.getRegionPageId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! Collage Region Data ");
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteCollageRegionData(SQLiteDatabase db, CollageRegionData item) {
        try {
            db.delete(TABLE_NAME, REGION_ID + " =? AND " + REGION_PAGE_ID + " =? ", new String[]{"" + item.getRegionId(), item.getRegionPageId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteCollageRegionDataByPage(SQLiteDatabase db, String pageId) {
        try {
            db.delete(TABLE_NAME, REGION_PAGE_ID + " = ? ", new String[]{pageId});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllCollageRegionData(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(regionId);
        out.writeString(pageId);
        out.writeFloat(imageLeft);
        out.writeFloat(imageTop);
        out.writeFloat(imageScale);
        out.writeString(imageUrl);
    }

    public boolean equals(Object obj) {
        return obj instanceof CollageRegionData && regionId == ((CollageRegionData) obj).regionId;
    }

    public long getRowId() {
        return this.id;
    }

    public CollageRegionData setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public CollageRegionData setRowId(long id) {
        this.id = id;

        return this;
    }

    public CollageRegionData putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public int getRegionId() {
        return this.regionId;
    }

    public CollageRegionData setRegionId(Cursor c) {
        this.regionId = i(c, REGION_ID);

        return this;
    }

    public CollageRegionData setRegionId(int id) {
        this.regionId = id;

        return this;
    }

    public CollageRegionData putRegionId(ContentValues v) {
        v.put(REGION_ID, this.regionId);

        return this;
    }

    public String getRegionPageId() {
        return this.pageId;
    }

    public CollageRegionData setRegionPageId(Cursor c) {
        this.pageId = s(c, REGION_PAGE_ID);

        return this;
    }

    public CollageRegionData setRegionPageId(String pageId) {
        this.pageId = pageId;

        return this;
    }

    public CollageRegionData putRegionPageId(ContentValues v) {
        v.put(REGION_PAGE_ID, this.pageId);

        return this;
    }

    public CollageRegionData setRegionImageUrl(String url) {
        this.imageUrl = url;

        return this;
    }

    public String getRegionImageUrl() {
        return imageUrl;
    }

    public CollageRegionData setRegionImageUrl(Cursor c) {
        this.imageUrl = s(c, REGION_IMAGE_URI);

        return this;
    }

    public CollageRegionData putRegionImageUrl(ContentValues v) {
        v.put(REGION_IMAGE_URI, this.imageUrl);

        return this;
    }

//    public File getImageFile() {
//        return imageFile;
//    }

    public Float getImageLeft() {
        return imageLeft;
    }

    public CollageRegionData setImageLeft(Cursor c) {
        this.imageLeft = f(c, REGION_IMAGE_LEFT);

        return this;
    }

    public CollageRegionData setImageLeft(final float imageLeft) {
        this.imageLeft = imageLeft;

        return this;
    }

    public CollageRegionData putImageLeft(ContentValues v) {
        v.put(REGION_IMAGE_LEFT, this.imageLeft);

        return this;
    }

    public Float getImageTop() {
        return imageTop;
    }

    public CollageRegionData setImageTop(Cursor c) {
        this.imageTop = f(c, REGION_IMAGE_TOP);

        return this;
    }

    public CollageRegionData setImageTop(final float imageTop) {
        this.imageTop = imageTop;

        return this;
    }

    public CollageRegionData putImageTop(ContentValues v) {
        v.put(REGION_IMAGE_TOP, this.imageTop);

        return this;
    }

    public float getImageScale() {
        return imageScale;
    }

    public CollageRegionData setImageScale(Cursor c) {
        this.imageScale = f(c, REGION_IMAGE_SCALE);

        return this;
    }

    public CollageRegionData setImageScale(final float imageScale) {
        this.imageScale = imageScale;

        return this;
    }

    public CollageRegionData putImageScale(ContentValues v) {
        v.put(REGION_IMAGE_SCALE, this.imageScale);

        return this;
    }

//    @Override
//    public boolean equals(final Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (!(o instanceof CollageRegionData)) {
//            return false;
//        }
//
//        final CollageRegionData collageRegionData = (CollageRegionData) o;
//
//        return imageFile.equals(collageRegionData.imageFile);
//    }
//
//    @SuppressWarnings("NonFinalFieldReferencedInHashCode")
//    @Override
//    public int hashCode() {
//        return imageFile.hashCode();
//    }
}