package com.jiran.coolbooks.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.Parcel;

import com.jiran.coolbooks.base.BaseComparator;
import com.jiran.coolbooks.common.FileUtil;
import com.jiran.coolbooks.common.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PageItem extends TableObject {
    public static final String TABLE_NAME = "page";

    public static final String ROW_ID = "_id";
    public static final String PAGE_ID = "page_id";
    public static final String PROJECT_ID = "project_id";
    public static final String PAGE_INDEX = "page_index";
    public static final String PAGE_COVER = "page_cover";
    public static final String PAGE_FRAME_COLOR = "page_frame_color";
    public static final String PAGE_COLLAGE_INDEX = "page_collage_index";
    public static final String PAGE_THUMBNAIL = "page_thumbnail";
    public static final String PAGE_FILE_URL = "page_file_url";
    public static final Creator<PageItem> CREATOR = new Creator<PageItem>() {
        @Override
        public PageItem createFromParcel(Parcel source) {
            return new PageItem(source);
        }

        @Override
        public PageItem[] newArray(int size) {
            return new PageItem[size];
        }
    };
    public static PageComparator COMPARATOR_INDEX_ASC = new PageIndexComparator(true);
    public static PageComparator COMPARATOR_INDEX_DESC = new PageIndexComparator(false);

    private long id;
    private String pageId;
    private String projectId;
    private int index;
    private boolean cover;
    private int frameColor;
    private int collageIndex;
    private byte[] thumbnail;
    private String fileUrl;

    public PageItem() {
        super();
    }

    public PageItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.pageId = in.readString();
        this.projectId = in.readString();
        this.index = in.readInt();
        this.cover = in.readInt() == 1;
        this.frameColor = in.readInt();
        this.collageIndex = in.readInt();
        in.readByteArray(this.thumbnail);
        this.fileUrl = in.readString();
    }

    public static void createTablePage(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(PAGE_ID + " text not null unique, ")
                .append(PROJECT_ID + " text , ")
                .append(PAGE_INDEX + " integer, ")
                .append(PAGE_COVER + " integer, ")
                .append(PAGE_FRAME_COLOR + " integer, ")
                .append(PAGE_THUMBNAIL + " BLOB, ")
                .append(PAGE_FILE_URL + " text , ")
                .append(PAGE_COLLAGE_INDEX + " integer) ")
                .toString());
    }

    public static PageItem getPageItem(SQLiteDatabase db, String id) {
        List<PageItem> list = getPageItems(db, null, PAGE_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<PageItem> getPageItemByProject(SQLiteDatabase db, String projectId) {
        List<PageItem> list = getPageItems(db, null, PROJECT_ID + " = ?", new String[]{projectId}, null, null, null, null);

        return list;
    }

    public static List<PageItem> getPageItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<PageItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                PageItem item = new PageItem()
                        .setRowId(c)
                        .setPageId(c)
                        .setProjectId(c)
                        .setIndex(c)
                        .setCover(c)
                        .setFrameColor(c)
                        .setCollageIndex(c)
                        .setPageFileUrl(c)
                        .setThumbnail(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdatePageItem(SQLiteDatabase db, PageItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putPageId(v);
            item.putProjectId(v);
            item.putIndex(v);
            item.putCover(v);
            item.putFrameColor(v);
            item.putCollageIndex(v);
            item.putPageFileUrl(v);
            item.putThumbnail(v);

            int rowAffected = db.update(TABLE_NAME, v, PAGE_ID + " =? ", new String[]{item.getPageId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getIndex());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deletePageItem(SQLiteDatabase db, PageItem item) {
        try {
            String fileName = item.getPageFileUrl();
            db.delete(TABLE_NAME, PAGE_ID + " = ? ", new String[]{item.getPageId()});
            FileUtil.delete(new File(fileName));

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deletePageItems(SQLiteDatabase db, ArrayList<PageItem> list) {
        for (PageItem item : list)
            deletePageItem(db, item);
        return false;
    }

    public static boolean deletePageItemByProject(SQLiteDatabase db, String projectId) {
        try {
            db.delete(TABLE_NAME, PROJECT_ID + " = ?", new String[]{projectId});

            String strFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + projectId;
            FileUtil.delete(new File(strFolderPath));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllPageItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(pageId);
        out.writeString(projectId);
        out.writeInt(index);
        out.writeInt(cover ? 1 : 0);
        out.writeInt(frameColor);
        out.writeInt(collageIndex);
        out.writeByteArray(thumbnail);
        out.writeString(fileUrl);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PageItem && pageId == ((PageItem) obj).pageId;
    }

    public long getRowId() {
        return this.id;
    }

    public PageItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public PageItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public PageItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getPageId() {
        return this.pageId;
    }

    public PageItem setPageId(Cursor c) {
        this.pageId = s(c, PAGE_ID);

        return this;
    }

    public PageItem setPageId(String id) {
        this.pageId = id;
        return this;
    }

    public PageItem putPageId(ContentValues v) {
        v.put(PAGE_ID, this.pageId);

        return this;
    }

    public String getProjectId() {
        return this.getProjectId();
    }

    public PageItem setProjectId(Cursor c) {
        this.projectId = s(c, PROJECT_ID);

        return this;
    }

    public PageItem setProjectId(String id) {
        this.projectId = id;

        return this;
    }

    public PageItem putProjectId(ContentValues v) {
        v.put(PROJECT_ID, this.projectId);

        return this;
    }

    public int getIndex() {
        return this.index;
    }

    public PageItem setIndex(Cursor c) {
        this.index = i(c, PAGE_INDEX);

        return this;
    }

    public PageItem setIndex(int index) {
        this.index = index;

        return this;
    }

    public PageItem putIndex(ContentValues v) {
        v.put(PAGE_INDEX, this.index);

        return this;
    }

    public boolean isCover() {
        return cover;
    }

    public PageItem setCover(Cursor c) {
        this.cover = i(c, PAGE_COVER) == 1;

        return this;
    }

    public PageItem setCover(boolean cover) {
        this.cover = cover;

        return this;
    }

    public PageItem putCover(ContentValues v) {
        v.put(PAGE_COVER, cover ? 1 : 0);

        return this;
    }

    public int getFrameColor() {
        return this.frameColor;
    }

    public PageItem setFrameColor(Cursor c) {
        this.frameColor = i(c, PAGE_FRAME_COLOR);

        return this;
    }

    public PageItem setFrameColor(int color) {
        this.frameColor = color;

        return this;
    }

    public PageItem putFrameColor(ContentValues v) {
        v.put(PAGE_FRAME_COLOR, this.frameColor);

        return this;
    }

    public int getCollageIndex() {
        return this.collageIndex;
    }

    public PageItem setCollageIndex(Cursor c) {
        this.collageIndex = i(c, PAGE_COLLAGE_INDEX);

        return this;
    }

    public PageItem setCollageIndex(int index) {
        this.collageIndex = index;

        return this;
    }

    public PageItem putCollageIndex(ContentValues v) {
        v.put(PAGE_COLLAGE_INDEX, this.collageIndex);

        return this;
    }

//    public byte[] getThumbnail() {
//        return thumbnail;
//    }

    public PageItem setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public PageItem setThumbnail(Cursor c) {
        this.thumbnail = blob(c, PAGE_THUMBNAIL);

        return this;
    }

    public PageItem putThumbnail(ContentValues v) {
        v.put(PAGE_THUMBNAIL, this.thumbnail);

        return this;
    }

    public String getPageFileUrl() {
        return this.fileUrl;
    }

    public PageItem setPageFileUrl(Cursor c) {
        this.fileUrl = s(c, PAGE_FILE_URL);

        return this;
    }

    public PageItem setPageFileUrl(String url) {
        this.fileUrl = url;

        return this;
    }

    public PageItem putPageFileUrl(ContentValues v) {
        v.put(PAGE_FILE_URL, this.fileUrl);

        return this;
    }

    private static class PageComparator extends BaseComparator implements Comparator<PageItem> {
        public PageComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(PageItem lhs, PageItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }

        protected final int compareId(PageItem lhs, PageItem rhs) {
            return compareLong(lhs.getRowId(), rhs.getRowId(), false, true);
        }
    }

    private static class PageIndexComparator extends PageComparator {
        public PageIndexComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(PageItem lhs, PageItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareInt(lhs.getIndex(), rhs.getIndex(), true);
            if (result != 0)
                return result;
            else
                return compareString(lhs.getPageId(), rhs.getPageId(), false);
        }
    }
}
