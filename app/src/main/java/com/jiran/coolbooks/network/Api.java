package com.jiran.coolbooks.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.jiran.coolbooks.BaseApplication;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.common.DBController;
import com.jiran.coolbooks.common.LogUtil;
import com.jiran.coolbooks.common.SPController;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class Api extends Handler {

    private static RequestQueue requestQueue;
    private static Queue<Message> messageQueue = new LinkedList<Message>();

    private WeakReference<ApiListener> ref;
    private BaseApplication app;
    private SPController sp;
    private DBController db;
    private BaseApplication.ResourceWrapper r;

    public Api(BaseApplication application, ApiListener target) {
        app = application;
        ref = new WeakReference<ApiListener>(target);
        sp = application.getSPController();
        db = application.getDBController();
        r = application.getResourceWrapper();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(app.getApplicationContext());
    }

    public static void cancelApprequest() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void setTarget(ApiListener target) {
        if (ref != null)
            ref.clear();

        ref = new WeakReference<ApiListener>(target);
    }

    public void publishPhotoBook(String cover, ArrayList<String> files, String title, String author, String publisher) {
        Message m = newMessage(Common.API_CODE_PUBLISH_FILES, null, false);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_SITE_ID, "www");
            params.put(Common.PARAM_USER_ID, sp.getLoginedUserId());
            params.put(Common.PARAM_TITLE, title);
            params.put(Common.PARAM_AUTHOR, author);
            params.put(Common.PARAM_PUBLISHER, publisher);
            params.put(Common.PARAM_TEMPLATE_NAME, "default");
            params.put(Common.PARAM_EBOOK_WIDTH, "1080");
            params.put(Common.PARAM_EBOOK_HEIGHT, "1080");
            params.put(Common.PARAM_COVER_WIDTH, "1080");
            params.put(Common.PARAM_COVER_HEIGHT, "1080");
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.API_BASE_URL + Common.API_PUBLISH_FILES, cover, files, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.ARG_SUCCESS);
                String message = j_result.optString(Common.TAG_MESSAGE);
                m.getData().putString(Common.TAG_MESSAGE, message);
                m.getData().putBoolean(Common.ARG_RESULT, res);

            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }


    private Message newMessage(int apiCode, String progressMessage, boolean showErrorMessage) {
        Message m = Message.obtain(this, apiCode);
        Bundle b = new Bundle();
        b.putString(Common.PARAM_PROGRESS_MESSAGE, progressMessage);
        b.putBoolean(Common.PARAM_SHOW_ERROR_MESSAGE, showErrorMessage);
        m.setData(b);

        return m;
    }

    @Override
    public void handleMessage(Message msg) {
        if (ref == null || ref.get() == null)
            return;

        ref.get().handleApiMessage(msg);
    }

    public static interface ApiListener {
        public void handleApiMessage(Message m);

        public BaseActivity getApiActivity();

        public FragmentManager getApiFragmentManager();
    }

    private abstract static class ApiRequest implements Response.Listener<String>, Response.ErrorListener {
        protected Api api;
        protected Message m;
        protected boolean addAuthorization;
        protected SimpleMultiPartRequest request;
        protected String paramString;

        public ApiRequest(Api _api, Message _m, int method, String _url, final String cover, final ArrayList<String> pages, final Map<String, String> _params, final boolean addAuthorizationToken) {
            this.api = _api;
            this.m = _m;
            this.addAuthorization = addAuthorizationToken;

            request = new SimpleMultiPartRequest(method, _url, this, this) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();
                    if (headers == null || headers.equals(Collections.emptyMap()))
                        headers = new Hashtable<String, String>();

                    headers.put(Common.HEADER_CACHE_CONTROL, Common.HEADER_NO_CACHE);
                    headers.put(Common.HEADER_APPLICATION_VERSION, api.app.getVersion());
                    headers.put(Common.HEADER_OS, api.app.getOS());
                    headers.put(Common.HEADER_CONTENT_TYPE, Common.HEADER_MULTIPART_FORM_DATA);
//                    if (addAuthorizationToken)
//                        headers.put(Common.HEADER_ACCESS_TOKEN, api.sp.getAccessToken());

                    if (LogUtil.DEBUG_MODE) {
                        StringBuilder sb = new StringBuilder();
                        for (String key : headers.keySet()) {
                            sb.append(key)
                                    .append(": ")
                                    .append(headers.get(key))
                                    .append("\n");
                        }
                        LogUtil.log("API_REQUEST", "HEADER===\n" + sb.toString());
                    }
                    return headers;
                }

            };

            request.addStringParam(Common.PARAM_SITE_ID, _params.get(Common.PARAM_SITE_ID));
            request.addStringParam(Common.PARAM_USER_ID, _params.get(Common.PARAM_USER_ID));
            request.addStringParam(Common.PARAM_TITLE, _params.get(Common.PARAM_TITLE));
            request.addStringParam(Common.PARAM_AUTHOR, _params.get(Common.PARAM_AUTHOR));
            request.addStringParam(Common.PARAM_PUBLISHER, _params.get(Common.PARAM_PUBLISHER));
            request.addStringParam(Common.PARAM_TEMPLATE_NAME, _params.get(Common.PARAM_TEMPLATE_NAME));
            request.addStringParam(Common.PARAM_EBOOK_WIDTH, _params.get(Common.PARAM_EBOOK_WIDTH));
            request.addStringParam(Common.PARAM_EBOOK_HEIGHT, _params.get(Common.PARAM_EBOOK_HEIGHT));
            request.addStringParam(Common.PARAM_COVER_WIDTH, _params.get(Common.PARAM_COVER_WIDTH));
            request.addStringParam(Common.PARAM_COVER_HEIGHT, _params.get(Common.PARAM_COVER_HEIGHT));

            request.addFile(Common.PARAM_COVER_FILE, cover);
            for (int i = 0; i < pages.size(); i++) {
                request.addFile(Common.PARAM_PAGE_FILE_ + i, pages.get(i));
            }
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(Common.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (LogUtil.DEBUG_MODE) {
                LogUtil.log("API_REQUEST", "URL : " + request.getUrl());

            }
        }

        abstract public void onSuccess(String result);

        abstract public void onSuccess(JSONObject j_result);

        abstract public void onError();

        @Override
        public void onResponse(String response) {
            if (response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }

//            try {
//                response = new String(response.getBytes("8859_1"), "utf-8");
//                LogUtil.log("UTF-8 " + response);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            if (LogUtil.DEBUG_MODE && response != null)
                LogUtil.log("API_REQUEST", "RESPONSE : " + response.toString());

//            if (m.what == Common.API_CODE_GET_TREND_LIST_WORLDWIDE
//                    || m.what == Common.API_CODE_GET_TREND_LEARNMORE
//                    || m.what == Common.API_CODE_GET_REPORT_LEARNMORE) {
//                onReceiveResponse(null, response, null);
//            } else {
            JSONObject j_response = null;
            try {
                j_response = new JSONObject(response);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (j_response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }
            onReceiveResponse(j_response, null, null);
//            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;

            if (error.networkResponse == null)
                m.arg2 = Common.RESULT_CODE_ERROR_UNKNOWN;
            else
                m.arg2 = error.networkResponse.statusCode;

            onReceiveResponse(null, null, String.valueOf(m.arg2));
        }

        public void ajax() {
            Api.requestQueue.add(request);
            synchronized (messageQueue) {
                messageQueue.add(m);
            }
        }

        private void onReceiveResponse(JSONObject j_result, String result, String errorCode) {
            synchronized (messageQueue) {
                messageQueue.remove(m);
            }

            new ResponseTask(this, j_result, result, errorCode).run();
        }
    }

    private static class ResponseTask extends AsyncTask<Void, Void, Void> {
        protected ApiRequest request;
        protected JSONObject j_result;
        protected String result;
        protected String errorCode;

        ResponseTask(ApiRequest request, JSONObject j_result, String result, String errorCode) {
            this.request = request;
            this.j_result = j_result;
            this.result = result;
            this.errorCode = errorCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (j_result != null)
                request.onSuccess(j_result);
            else if (result != null)
                request.onSuccess(result);
            else
                request.onError();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            request.api.sendMessage(request.m);
        }

        public void run() {
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        }
    }
}
