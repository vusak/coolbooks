package com.jiran.coolbooks.base;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.data.CollageRegion;
import com.jiran.coolbooks.data.CollageRegionData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

public class CollageItemView extends AppCompatImageView {

    private static final float MIN_SCALE_FACTOR = 1f;
    private static final float MAX_SCALE_FACTOR = 5f;

    private CollageRegion mCollageRegion;
    private CollageRegionData mRegionData;
    private File mRegionFile;

    private OnClickListener mOnClickListener;
    private GestureDetectorCompat mGestureDetector;
    private ScaleGestureDetector mScaleGestureDetector;

    private boolean mNeedInit = true;

    private float mMinScale;
    private float mMaxScale;
    private ScrollHolder mScrollHolder;

    private Context context;

    public CollageItemView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CollageItemView(Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public CollageItemView(Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void init() {
        mScaleGestureDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            private static final float HALF = 0.5f;

            @Override
            public boolean onScale(final ScaleGestureDetector detector) {
                final float oldScaleFactor = mRegionData.getImageScale();
                // we use "+" but "*" because here it is more suitable I think
                float newScaleFactor = mRegionData.getImageScale() + (detector.getScaleFactor() - 1) * 0.7f;
                // Don't let the object get too small or too large.
                newScaleFactor = Math.max(MIN_SCALE_FACTOR, Math.min(newScaleFactor, MAX_SCALE_FACTOR));

                if (0 != Float.compare(oldScaleFactor, newScaleFactor)) {
                    updateImagePosition(mMinScale * oldScaleFactor, mMinScale * newScaleFactor);
                    mRegionData.setImageScale(newScaleFactor);
                    updateScale();
                }

                return super.onScale(detector);
            }

            private void updateImagePosition(final float oldImageScaleFactor, final float newImageScaleFactor) {
                final float imageCenterX;
                final float imageCenterY;
                if (null == mRegionData.getImageLeft() || null == mRegionData.getImageTop()) {
                    imageCenterX = (mScrollHolder.mScrollXMax + getWidth()) * HALF;
                    imageCenterY = (mScrollHolder.mScrollYMax + getHeight()) * HALF;
                } else {
                    imageCenterX = mScrollHolder.mScrollXMax * mRegionData.getImageLeft() + getWidth() * HALF;
                    imageCenterY = mScrollHolder.mScrollYMax * mRegionData.getImageTop() + getHeight() * HALF;
                }
                final float newImageCenterX = imageCenterX * newImageScaleFactor / oldImageScaleFactor;
                final float newImageCenterY = imageCenterY * newImageScaleFactor / oldImageScaleFactor;

                final ScrollHolder scrollHolder = generateScrollHolder(newImageScaleFactor);

                mRegionData.setImageLeft(scrollHolder.mScrollXMax == 0 ? 0 :
                        Math.max(0, Math.min(scrollHolder.mScrollXMax, (newImageCenterX - getWidth() * HALF) / scrollHolder.mScrollXMax)));
                mRegionData.setImageTop(scrollHolder.mScrollYMax == 0 ? 0 :
                        Math.max(0, Math.min(scrollHolder.mScrollYMax, (newImageCenterY - getHeight() * HALF) / scrollHolder.mScrollYMax)));
            }

            @Override
            public boolean onScaleBegin(final ScaleGestureDetector detector) {
                return null != mScrollHolder;
            }
        });
        mGestureDetector = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown(final MotionEvent e) {
                return null != mOnClickListener || null != mScrollHolder;
            }

            @Override
            public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX, final float distanceY) {
                if (null != mScrollHolder) {
                    final float curScrollX = getScrollX();
                    final float curScrollY = getScrollY();

                    scrollTo((int) (curScrollX + distanceX), (int) (curScrollY + distanceY));
                }
                return true;
            }

            @Override
            public boolean onSingleTapUp(final MotionEvent e) {
                if (null != mOnClickListener) {
                    mOnClickListener.onClick(CollageItemView.this);
                }
                return true;
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        boolean handle = false;
        if (null != mScrollHolder) {
            handle = mScaleGestureDetector.onTouchEvent(event);
        }
        handle |= mGestureDetector.onTouchEvent(event);
        return handle;
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (oldw != 0 && oldh != 0) {
            mNeedInit = true;
        }

        new Handler().post(() -> {
            if (mNeedInit) {
                setRegionData(context, mRegionData, false);
            }
        });
    }

    @Override
    public void setOnClickListener(final OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public CollageRegion getCollageRegion() {
        return mCollageRegion;
    }

    public void setCollageRegion(final CollageRegion collageRegion) {
        mCollageRegion = collageRegion;
    }

    public void setRegionData(Context context, final CollageRegionData regionData, boolean save) {
        if (mRegionData != regionData) {
            mNeedInit = true;
            mRegionData = regionData;
            updateRegionData(save);
        } else if (mNeedInit || save) {
            updateRegionData(save);
        }
    }

    private void updateRegionData(boolean save) {
        mScrollHolder = null;
        setImageMatrix(null);

        if (null == mRegionData) {
            mNeedInit = false;
            setScaleType(ScaleType.CENTER_INSIDE);
            setBackgroundResource(save ? R.color.white_a100 : R.color.gray01_a100);
            setImageResource(save ? R.color.white_a100 : R.drawable.ic_add_blue_36);
        } else {
            setScaleType(ScaleType.FIT_XY);
            final int width = getWidth();
            final int height = getHeight();

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            mRegionFile = getFile(Uri.parse(mRegionData.getRegionImageUrl()));
            if (mRegionFile == null)
                return;
            BitmapFactory.decodeFile(mRegionFile.getPath(), options);

            if (0 == width || 0 == height || 0 == options.outWidth || 0 == options.outHeight) {
                return;
            }
            mNeedInit = false;

            final float scale = Math.max(1f * width / options.outWidth, 1f * height / options.outHeight);
            final int resultWidth;
            final int resultHeight;
            if (scale < 1) {
                resultWidth = (int) (options.outWidth * scale);
                resultHeight = (int) (options.outHeight * scale);
            } else {
                resultWidth = (int) (options.outWidth * Math.ceil(scale));
                resultHeight = (int) (options.outHeight * Math.ceil(scale));
            }

            Picasso.with(getContext()).load(mRegionFile)
                    .resize(resultWidth, resultHeight)
                    .error(R.drawable.background_collage_empty).into(this, new Callback() {
                        @Override
                        public void onSuccess() {
                            final int width = getWidth();
                            final int height = getHeight();
                            if (0 == width || 0 == height) {
                                return;
                            }
                            final Drawable drawable = getDrawable();
                            if (null == drawable) {
                                return;
                            }
                            final int bitmapWidth = drawable.getIntrinsicWidth();
                            final int bitmapHeight = drawable.getIntrinsicHeight();
                            if (bitmapWidth < 0 || bitmapHeight < 0) {
                                return;
                            }

                            mMinScale = 1f * Math.max(1f * width / bitmapWidth, 1f * height / bitmapHeight);
                            mMaxScale = mMinScale * MAX_SCALE_FACTOR;

                            updateScale();
                        }

                        @Override
                        public void onError() {
                            // pass
                        }
                    }
            );
        }
    }

    private void updateScale() {
        final float scaleFactor = mMinScale * mRegionData.getImageScale();
        mScrollHolder = generateScrollHolder(scaleFactor);
        final Matrix matrix = new Matrix();
        matrix.setScale(scaleFactor, scaleFactor);
        setScaleType(ScaleType.MATRIX);
        setImageMatrix(matrix);

        if (mRegionData.getImageLeft() == null || mRegionData.getImageTop() == null) {
            scrollTo(mScrollHolder.mScrollXMax / 2, mScrollHolder.mScrollYMax / 2);
        } else {
            CollageItemView.super.scrollTo((int) (mRegionData.getImageLeft() * mScrollHolder.mScrollXMax),
                    (int) (mRegionData.getImageTop() * mScrollHolder.mScrollYMax));
        }
    }

    private ScrollHolder generateScrollHolder(final float scaleFactor) {
        final int width = getWidth();
        final int height = getHeight();
        if (0 == width || 0 == height) {
            return ScrollHolder.EMPTY;
        }
        final Drawable drawable = getDrawable();
        if (null == drawable) {
            return ScrollHolder.EMPTY;
        }
        final int bitmapWidth = drawable.getIntrinsicWidth();
        final int bitmapHeight = drawable.getIntrinsicHeight();
        if (bitmapWidth < 0 || bitmapHeight < 0) {
            return ScrollHolder.EMPTY;
        }

        final int scrollXMax = Math.abs(Math.round(width - bitmapWidth * scaleFactor));
        final int scrollYMax = Math.abs(Math.round(height - bitmapHeight * scaleFactor));
        return new ScrollHolder(scrollXMax, scrollYMax);
    }

    @Override
    public void scrollTo(final int x, final int y) {
        int newScrollX = x;
        int newScrollY = y;
        if (null != mScrollHolder) {
            if (newScrollX > mScrollHolder.mScrollXMax) {
                newScrollX = mScrollHolder.mScrollXMax;
            }
            if (newScrollX < 0) {
                newScrollX = 0;
            }
            if (newScrollY > mScrollHolder.mScrollYMax) {
                newScrollY = mScrollHolder.mScrollYMax;
            }
            if (newScrollY < 0) {
                newScrollY = 0;
            }

            mRegionData.setImageLeft(mScrollHolder.mScrollXMax == 0 ? 0 : 1f * newScrollX / mScrollHolder.mScrollXMax);
            mRegionData.setImageTop(mScrollHolder.mScrollYMax == 0 ? 0 : 1f * newScrollY / mScrollHolder.mScrollYMax);
        }

        super.scrollTo(newScrollX, newScrollY);
    }

    private File getFile(Uri uri) {
        String path = getRealPathFromURI(uri);

        return TextUtils.isEmpty(path) ? null : new File(path);
    }

    @SuppressLint("newAPI")
    private String getRealPathFromURI(Uri contentUri) {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), contentUri)) {
            if (isExternalStorageDocument(contentUri)) {
                final String docId = DocumentsContract.getDocumentId(contentUri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(contentUri)) {
                final String id = DocumentsContract.getDocumentId(contentUri);
                contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(contentUri)) {
                final String docId = DocumentsContract.getDocumentId(contentUri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(contentUri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(contentUri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(contentUri.getScheme())) {
            return contentUri.getPath();
        }
        return null;
//        if (contentUri.getPath().startsWith("/storage")) {
//            return contentUri.getPath();
//        }
//        String id = DocumentsContract.getDocumentId(contentUri).split(":")[1];
//        String[] columns = {MediaStore.Files.FileColumns.DATA};
//        String selection = MediaStore.Files.FileColumns._ID + " = " + id;
//        Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"), columns, selection, null, null);
//        try {
//            int columnIndex = cursor.getColumnIndex(columns[0]);
//            if (cursor.moveToFirst()) {
//                return cursor.getString(columnIndex);
//            }
//        } finally {
//            cursor.close();
//        }
//        return null;
    }

    private static class ScrollHolder {

        static final ScrollHolder EMPTY = new ScrollHolder();

        int mScrollXMax;
        int mScrollYMax;

        ScrollHolder(final int scrollXMax, final int scrollYMax) {
            mScrollXMax = scrollXMax;
            mScrollYMax = scrollYMax;
        }

        ScrollHolder() {
        }
    }
}