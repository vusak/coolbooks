package com.jiran.coolbooks.base;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.data.Collage;
import com.jiran.coolbooks.data.CollageFillData;
import com.jiran.coolbooks.data.CollageRegion;
import com.jiran.coolbooks.data.CollageRegionData;

/**
 * General view that construct all regions as CollageItemView children. This view calculate there sizes and location.
 * Can handle region clicks.
 * <p/>
 * Date: 4/8/2014
 * Time: 5:23 PM
 *
 * @author MiG35
 */
@SuppressWarnings("UnusedDeclaration")
public class CollageViewGroup extends SquareRelativeLayout {

    private CollageFillData mCollageFillData;
    private Context context;
    private RegionClickListener mRegionClickListener;
    private final OnClickListener mCollageItemClickListener = new OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (null != mRegionClickListener && v instanceof CollageItemView) {
                mRegionClickListener.onRegionClicked(((CollageItemView) v).getCollageRegion());
            }
        }
    };

    public CollageViewGroup(Context context) {
        super(context);
        context = context;
    }

    public CollageViewGroup(Context context, final AttributeSet attrs) {
        super(context, attrs);
        context = context;
    }

    public CollageViewGroup(Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        context = context;
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        new Handler().post(() -> {
            setCollage(mCollageFillData);
        });
    }

    public void setCollage(final Collage collage) {
        setCollage(null == collage ? null : new CollageFillData(collage));
    }

    public void setCollage(final CollageFillData collageFillData) {
        if (mCollageFillData != collageFillData || getChildCount() != collageFillData.getRegionsCount()) {
            removeAllViews();
            mCollageFillData = collageFillData;
            initCollageViews();
        }
    }

    public void invalidateRegionData(boolean save) {
        for (int i = 0; i < getChildCount(); ++i) {
            final View child = getChildAt(i);
            if (child instanceof CollageItemView) {
                final CollageItemView collageItemView = (CollageItemView) child;
                final CollageRegionData regionData = mCollageFillData.getRegionData(collageItemView.getCollageRegion());
                collageItemView.setRegionData(context, regionData, save);

                if (mRegionClickListener != null)
                    mRegionClickListener.onRegionDataInvalidated(i, regionData);
            }
        }

        new Handler().postDelayed(() -> {
            if (mRegionClickListener != null)
                mRegionClickListener.onRegionDataInvalidated();
        }, 100);

    }

    @SuppressWarnings("ConstantConditions")
    private void updateClickEnable() {
        final boolean clicksEnabled = mRegionClickListener != null;
        for (int i = 0; i < getChildCount(); ++i) {
            final View child = getChildAt(i);
            if (clicksEnabled) {
                child.setOnClickListener(mCollageItemClickListener);
            } else {
                child.setOnClickListener(null);
            }
        }
    }

    @SuppressWarnings("ObjectAllocationInLoop")
    private void initCollageViews() {
        setBackgroundColor(getContext().getResources().getColor(R.color.white_a100));

        final int width = getWidth();
        final int height = getHeight();
        if (null == mCollageFillData || width == 0 || height == 0) {
            return;
        }
        for (final CollageRegion collageRegion : mCollageFillData.getCollageRegions()) {
            final int collageWidth = (int) Math.round(collageRegion.getWidth() * width);
            final int collageHeight = (int) Math.round(collageRegion.getHeight() * height);

            final LayoutParams layoutParams = new LayoutParams(collageWidth, collageHeight);
            layoutParams.setMargins((int) Math.round(width * collageRegion.getLeft()), (int) Math.round(height * collageRegion.getTop()), 0, 0);
            final CollageItemView collageItemView = new CollageItemView(getContext());
            collageItemView.setCollageRegion(collageRegion);
            addView(collageItemView, layoutParams);
        }
        updateClickEnable();
        invalidateRegionData(false);
    }

    public void setRegionClickListener(final RegionClickListener regionClickListener) {
        mRegionClickListener = regionClickListener;
        updateClickEnable();
    }

    public interface RegionClickListener {

        void onRegionClicked(CollageRegion collageRegion);

        void onRegionDataInvalidated();

        void onRegionDataInvalidated(int id, CollageRegionData data);
    }
}