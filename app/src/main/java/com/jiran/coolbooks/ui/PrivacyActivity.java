package com.jiran.coolbooks.ui;

import android.os.Bundle;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.common.Common;

public class PrivacyActivity extends BaseActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_privacy);

        webView = fv(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onCloseWindow(WebView window) {
                super.onCloseWindow(window);
                finish();
            }
        });
        webView.loadUrl(Common.URL_PRIVACY_POLICY);
    }

}
