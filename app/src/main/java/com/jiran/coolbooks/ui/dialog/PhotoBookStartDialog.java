package com.jiran.coolbooks.ui.dialog;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseDialogFragment;
import com.jiran.coolbooks.common.Util;

public class PhotoBookStartDialog extends BaseDialogFragment {

    public static final String TAG = PhotoBookStartDialog.class.getSimpleName();

    TextView dlgTitleText;
    TextView titleText, authorText, publisherText;
    EditText titleEdit, authorEdit, publisherEdit;
    ImageButton titleDelButton, authorDelButton, publisherDelButton;
    View titleLine, authorLine, publisherLine;

    Button cancelButton, startButton;
    UploadingDialogListener listener;

    String title, author, publisher;

    public static PhotoBookStartDialog newInstance(String tag, String title, String author, String publisher) {
        PhotoBookStartDialog d = new PhotoBookStartDialog();

        d.createArguments(tag);
        d.title = title;
        d.author = author;
        d.publisher = publisher;
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_photobook_start;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        dlgTitleText = fv(R.id.text_dlg_title);
        dlgTitleText.setText(r.s(!TextUtils.isEmpty(title) ? R.string.title_photobook_detail_edit : R.string.title_photobook_detail));

        titleText = fv(R.id.text_photo_title);
        titleEdit = fv(R.id.edit_photo_title);
        titleEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEditStatus(R.id.edit_photo_title);
            }
        });
        titleEdit.setOnFocusChangeListener((v, hasFocus) -> setEditStatus(v.getId()));
        titleDelButton = fv(R.id.button_photo_del_title);
        titleDelButton.setOnClickListener(v -> titleEdit.setText(""));
        titleLine = fv(R.id.line_photo_title);

        authorText = fv(R.id.text_photo_author);
        authorEdit = fv(R.id.edit_photo_author);
        authorEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEditStatus(R.id.edit_photo_author);
            }
        });
        authorEdit.setOnFocusChangeListener((v, hasFocus) -> setEditStatus(v.getId()));
        authorDelButton = fv(R.id.button_photo_del_author);
        authorDelButton.setOnClickListener(v -> authorEdit.setText(""));
        authorLine = fv(R.id.line_photo_author);

        publisherText = fv(R.id.text_photo_publisher);
        publisherEdit = fv(R.id.edit_photo_publisher);
        publisherEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEditStatus(R.id.edit_photo_publisher);
            }
        });
        publisherEdit.setOnFocusChangeListener((v, hasFocus) -> setEditStatus(v.getId()));
        publisherDelButton = fv(R.id.button_photo_del_publisher);
        publisherDelButton.setOnClickListener(v -> publisherEdit.setText(""));
        publisherLine = fv(R.id.line_photo_publisher);

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(v -> {
            Util.hideKeyBoard(titleEdit);
            dismiss();
        });

        startButton = fv(R.id.button_positive);
        startButton.setOnClickListener(v -> {
            if (!checkEmpty() && listener != null) {
                listener.onStart(titleEdit.getText().toString(), authorEdit.getText().toString(), publisherEdit.getText().toString());
                dismiss();
            }
        });

        titleEdit.setText(TextUtils.isEmpty(title) ? "" : title);
        authorEdit.setText(TextUtils.isEmpty(author) ? "" : author);
        publisherEdit.setText(TextUtils.isEmpty(publisher) ? "" : publisher);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.8f;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            rate = 0.5f;

        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);

        titleEdit.post(() -> Util.showKeyBoard(titleEdit));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void setEditStatus(int id) {
        boolean titleFocused = id == R.id.edit_photo_title;
        boolean authorFocused = id == R.id.edit_photo_author;
        boolean publisherFocused = id == R.id.edit_photo_publisher;

        boolean titleEmpty = TextUtils.isEmpty(titleEdit.getText().toString());
        boolean authorEmpty = TextUtils.isEmpty(authorEdit.getText().toString());
        boolean publisherEmpty = TextUtils.isEmpty(publisherEdit.getText().toString());

        titleText.setTextColor(ContextCompat.getColor(getActivity(), titleFocused ? R.color.blue_a100 : R.color.gray6_a100));
        authorText.setTextColor(ContextCompat.getColor(getActivity(), authorFocused ? R.color.blue_a100 : R.color.gray6_a100));
        publisherText.setTextColor(ContextCompat.getColor(getActivity(), publisherFocused ? R.color.blue_a100 : R.color.gray6_a100));

        titleText.setVisibility(titleEmpty ? View.GONE : View.VISIBLE);
        authorText.setVisibility(authorEmpty ? View.GONE : View.VISIBLE);
        publisherText.setVisibility(publisherEmpty ? View.GONE : View.VISIBLE);

        titleEdit.setTextColor(ContextCompat.getColor(getActivity(), titleFocused ? R.color.black_a100 : R.color.gray6_a100));
        authorEdit.setTextColor(ContextCompat.getColor(getActivity(), authorFocused ? R.color.black_a100 : R.color.gray6_a100));
        publisherEdit.setTextColor(ContextCompat.getColor(getActivity(), publisherFocused ? R.color.black_a100 : R.color.gray6_a100));

        titleDelButton.setVisibility(titleFocused && !titleEmpty ? View.VISIBLE : View.GONE);
        authorDelButton.setVisibility(authorFocused && !authorEmpty ? View.VISIBLE : View.GONE);
        publisherDelButton.setVisibility(publisherFocused && !publisherEmpty ? View.VISIBLE : View.GONE);

        titleLine.setBackgroundResource(titleFocused ? R.color.blue_a100 : R.color.gray4_a100);
        authorLine.setBackgroundResource(authorFocused ? R.color.blue_a100 : R.color.gray4_a100);
        publisherLine.setBackgroundResource(publisherFocused ? R.color.blue_a100 : R.color.gray4_a100);

        titleLine.setVisibility(!titleFocused && titleEmpty ? View.GONE : View.VISIBLE);
        authorLine.setVisibility(!authorFocused && authorEmpty ? View.GONE : View.VISIBLE);
        publisherLine.setVisibility(!publisherFocused && publisherEmpty ? View.GONE : View.VISIBLE);
    }

    private boolean checkEmpty() {
        Util.hideKeyBoard(titleEdit);
        if (TextUtils.isEmpty(titleEdit.getText().toString())) {
            Util.showToast(getActivity(), r.s(R.string.message_empty_photo_title));
            Util.showKeyBoard(titleEdit);
            return true;
        }

        if (TextUtils.isEmpty(authorEdit.getText().toString())) {
            Util.showToast(getActivity(), r.s(R.string.message_empty_photo_author));
            Util.showKeyBoard(authorEdit);
            return true;
        }

        if (TextUtils.isEmpty(publisherEdit.getText().toString())) {
            Util.showToast(getActivity(), r.s(R.string.message_empty_photo_publisher));
            Util.showKeyBoard(publisherEdit);
            return true;
        }
        return false;
    }

    public PhotoBookStartDialog setListener(UploadingDialogListener listener) {
        this.listener = listener;

        return this;
    }

    public static interface UploadingDialogListener {
        public void onStart(String title, String author, String publisher);

        public void onCancel();
    }

}
