package com.jiran.coolbooks.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.widget.Button;
import android.widget.ImageView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.common.Common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PreviewActivity extends BaseActivity {
    private Bitmap previewBitmap;
    private ImageView previewImage;
    private Button saveButton;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        previewBitmap = (Bitmap) getIntent().getParcelableExtra("preview_data");
        initView();

        previewImage.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    previewImage.setImageBitmap(previewBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 100);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void handleApiMessage(Message m) {
        boolean success = m.getData().getBoolean(Common.ARG_RESULT);

        super.handleApiMessage(m);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView() {
        previewImage = fv(R.id.iv_collage);

        saveButton = fv(R.id.button_save);
        saveButton.setOnClickListener(v -> {
            FileOutputStream fos;
            String strFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/TEST";
            File folder = new File(strFolderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            String strFilePath = strFolderPath + "/" + System.currentTimeMillis() + ".png";
            File fileCacheItem = new File(strFilePath);

            try {
                fos = new FileOutputStream(fileCacheItem);
                previewBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            strFilePath = "file:///" + strFilePath;
            PreviewActivity.this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(strFilePath)));
        });
    }

}
