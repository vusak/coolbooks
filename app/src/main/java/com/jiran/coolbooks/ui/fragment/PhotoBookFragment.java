package com.jiran.coolbooks.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.adapter.PageAdapter;
import com.jiran.coolbooks.adapter.PreviewAdapter;
import com.jiran.coolbooks.adapter.ReAbstractViewHolder;
import com.jiran.coolbooks.base.BaseFragment;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.common.LogUtil;
import com.jiran.coolbooks.common.Util;
import com.jiran.coolbooks.data.PageItem;
import com.jiran.coolbooks.data.ProjectItem;
import com.jiran.coolbooks.network.Api;
import com.jiran.coolbooks.ui.CollageActivity;
import com.jiran.coolbooks.ui.dialog.MessageDialog;
import com.jiran.coolbooks.ui.dialog.PhotoBookStartDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

public class PhotoBookFragment extends BaseFragment {

    private static final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 10001;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 10002;
    private static String[] PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    LinearLayout emptyLayout, mainLayout;
    ArrayList<ProjectItem> projectList;
    ProjectItem projectItem;
    LinearLayout uploadingLayout;
    TextView guideText;
    ImageButton editModeButton;
    Button completeButton;
    ItemTouchHelper helper;
    ImageView previewImage;
    ViewPager viewPager;
    PreviewAdapter previewAdapter;
    PhotoBookFragmentListener listener;
    Api api;
    Toast toast;
    private LinearLayout newProjectButton, newPageButton, editPageButton;
    private LinearLayout createButton, guideLayout, publishButton;
    private RecyclerView pageLv;
    private PageAdapter pageAdapter;
    private LinearLayoutManager pageManager;
    private ArrayList<PageItem> pageList;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser)
            refreshList();
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
        askForPermission();

        refreshList();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_photobook;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1001:
                int index = data.getIntExtra(Common.INTENT_PAGE_INDEX, 0);

                if (pageList == null)
                    pageList = new ArrayList<>();
                pageList.clear();
                pageList.addAll(db.getPageItemByProject(projectItem.getProjectId()));
                if (index >= pageList.size())
                    index = 0;

                if (pageList != null && pageList.size() > 0) {
//                    selectedPageId = pageList.get(index).getPageId();
//                    selectedCollage = pageList.get(index).getCollageIndex();
//                    if (pageList.get(index).getThumbnail() != null)
//                        previewImage.setImageBitmap(ImageUtil.getImage(pageList.get(index).getThumbnail()));

                    if (pageAdapter != null) {
                        pageAdapter.notifyDataSetChanged();
                        pageAdapter.setSelectedIndex(index);
                    }
                }
                if (pageAdapter != null)
                    pageAdapter.refreshItems();
                pageLv.scrollToPosition(index);
                previewAdapter.notifyDataSetChanged();
                viewPager.setCurrentItem(index);
                break;
        }
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_PUBLISH_FILES) {
            uploadingLayout.setVisibility(View.GONE);
            boolean result = m.getData().getBoolean(Common.ARG_RESULT);
            String message = m.getData().getString(Common.TAG_MESSAGE);
            LogUtil.log(message);

            if (result) {
                if (projectItem != null) {
                    db.deletePageItemByProject(projectItem.getProjectId());
                    projectItem = null;
                }
                db.clearProject();
                refreshList();
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(result ? R.string.message_complete_uploading : R.string.message_fail_uploading));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.move));
                dlg.setPositiveListener((dialog, tag) -> {
                    if (listener != null)
                        listener.onOpenLibrary();
                });
                sdf(dlg);
            } else {
                Util.showToast(getActivity(), r.s(R.string.message_fail_uploading));
            }

        }
    }

    public void setListener(PhotoBookFragmentListener listener) {
        this.listener = listener;
    }

    private void initView() {
        emptyLayout = fv(R.id.layout_empty);
        mainLayout = fv(R.id.layout_main);

        newProjectButton = fv(R.id.button_new_project);
        newProjectButton.setOnClickListener(v -> {
            newProject();
        });

        createButton = fv(R.id.button_create);
        createButton.setOnClickListener(v -> {
            if (pageList != null && pageList.size() > 0) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.title_create_photo_book));
                dlg.setInfo(r.s(R.string.message_create_photo_book));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.create));
                dlg.setPositiveListener((dialog, tag) -> {
                    newProject();
                });
                sdf(dlg);
            } else {
                newProject();
            }

        });

        publishButton = fv(R.id.button_upload);
        publishButton.setOnClickListener(v -> {
            if (checkUpload()) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_upload));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.ok));
                dlg.setPositiveListener((dialog, tag) -> {
                    uploadingLayout.setVisibility(View.VISIBLE);
                    new PublishPhotoBook().execute(projectItem.getTitle(), projectItem.getAuthor(), projectItem.getPublisher());
                });
                sdf(dlg);
            }
        });

        editPageButton = fv(R.id.button_edit_page);
        editPageButton.setOnClickListener(v -> {
            if (pageList != null && pageAdapter != null) ;
            PageItem item = pageList.get(pageAdapter.getSelectedIndex());
            if (item != null) {
                Intent intent = new Intent(getActivity(), CollageActivity.class);
                intent.putExtra(Common.INTENT_PROJECT_ID, projectItem.getProjectId());
                intent.putExtra(Common.INTENT_PAGE_ID, item.getPageId());

                startActivityForResult(intent, 1001);
            }
        });

        newPageButton = fv(R.id.button_new_page);
        newPageButton.setOnClickListener(v -> {
            newPage();
        });

        pageList = new ArrayList<>();

        previewImage = fv(R.id.image_preview);
        viewPager = fv(R.id.viewpager);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            int lastPosition = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Util.showToast(getActivity(), position + "");
                if (lastPosition == position)
                    return;

                lastPosition = position;
                pageAdapter.setSelectedIndex(position);
                pageLv.scrollToPosition(position);
            }
        });
        previewAdapter = new PreviewAdapter(getActivity(), pageList);
        viewPager.setAdapter(previewAdapter);

        guideLayout = fv(R.id.layout_guide);

        pageLv = fv(R.id.lv_page);
        pageManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        pageLv.setLayoutManager(pageManager);
        pageAdapter = new PageAdapter(getActivity(), R.layout.item_page, pageList, new PageAdapter.PageAdapterListener() {
            @Override
            public void OnItemClick(int position, PageItem item) {
//                if (item.getThumbnail() != null)
//                    previewImage.setImageBitmap(ImageUtil.getImage(item.getThumbnail()));
                viewPager.setCurrentItem(position);
                pageAdapter.setSelectedIndex(position);
            }

            @Override
            public void OnItemLongClick(int position, PageItem item) {
                setEditMode(true);
            }

            @Override
            public void onDeleteButtonClick(int positon, PageItem item) {
                if (pageAdapter.isDeleteMode()) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_delete_page));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.delete));
                    dlg.setPositiveListener((dialog, tag) -> {
                        db.deleteCollageRegionDataByPage(item.getPageId());
                        db.deletePageItem(item);

                        refreshList();

                        for (int i = 0; i < pageList.size(); i++) {
                            PageItem page = pageList.get(i);
                            pageList.get(i).setIndex(i);
                            page.setIndex(i);
                            db.insertOrUpdatePageItem(page);
                        }

                    });
                    sdf(dlg);

                }
            }

            @Override
            public void onStartDrag(ReAbstractViewHolder holder) {
                helper.startDrag(holder);
            }
        });
        pageLv.setAdapter(pageAdapter);
        helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(pageLv);

        guideText = fv(R.id.text_guide);
        guideText.setText(r.s(R.string.title_edit_page_sel));

        editModeButton = fv(R.id.button_delete_mode);
        editModeButton.setOnClickListener(v -> {
            setEditMode(true);
        });

        completeButton = fv(R.id.button_complete);
        completeButton.setOnClickListener(v -> {
            setEditMode(false);
        });

        uploadingLayout = fv(R.id.layout_uploading);
        uploadingLayout.setVisibility(View.GONE);
    }

    /**
     * Permission
     */
    private boolean askForPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.app_name) + " needs permission.\nGo to Settings > Permissions, then allow the following permissions and try again:");
                    dlg.setPositiveLabel("Settings");
                    dlg.setPositiveListener((dialog, tag) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + getActivity().getPackageName()));
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    });
                    dlg.setNegativeLabel("cancel");
                    sdf(dlg);
                }

                return false;
            } else {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
//            case REQUEST_CODE_READ_EXTERNAL_STORAGE :
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;

            case REQUEST_CODE_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Util.showToast(getActivity(), "NEED PERMISSION CHECK");
                }
                return;

            default:
                break;
        }
    }

    private void newPage() {

        Intent intent = new Intent(getActivity(), CollageActivity.class);
        intent.putExtra(Common.INTENT_PROJECT_ID, projectItem.getProjectId());
        intent.putExtra(Common.INTENT_PAGE_ID, "");

        startActivityForResult(intent, 1001);
    }

    private void newProject() {
        if (projectItem != null) {
            db.deletePageItemByProject(projectItem.getProjectId());
            projectItem = null;
        }
        db.clearProject();
        refreshList();
//        PageItem pageItem = new PageItem();
//        pageItem.setPageId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
//        pageItem.setIndex(0);
//        pageItem.setCollageIndex(0);
//        pageItem.setProjectId(item.getProjectId());
//        db.insertOrUpdatePageItem(pageItem);
//        pageList.add(pageItem);

        PhotoBookStartDialog dlg = PhotoBookStartDialog.newInstance(PhotoBookStartDialog.TAG, "", "", "");
        dlg.setListener(new PhotoBookStartDialog.UploadingDialogListener() {
            @Override
            public void onStart(String title, String author, String publisher) {
                ProjectItem item = new ProjectItem();
                item.setProjectId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                item.setCreateDate(System.currentTimeMillis());
                item.setUserId(sp.getLoginedUserId());
                item.setTitle(title);
                item.setAuthor(author);
                item.setPublisher(publisher);
                db.insertOrUpdateProjectItem(item);
                projectItem = item;

                PageItem pageItem = new PageItem();
                pageItem.setPageId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                pageItem.setIndex(0);
                pageItem.setCollageIndex(CollageActivity.COLLAGE_COVER_TOP);
                pageItem.setProjectId(item.getProjectId());
                db.insertOrUpdatePageItem(pageItem);
                pageList.add(pageItem);

                Intent intent = new Intent(getActivity(), CollageActivity.class);
                intent.putExtra(Common.INTENT_PROJECT_ID, projectItem.getProjectId());
                intent.putExtra(Common.INTENT_PAGE_ID, pageItem.getPageId());

                startActivityForResult(intent, 1001);
            }

            @Override
            public void onCancel() {

            }
        });
        sdf(dlg);
//
    }

    private void refreshList() {
        if (projectList == null)
            projectList = new ArrayList<>();
        projectList.clear();
        projectList.addAll(db.getProjectItems());

        boolean empty = projectList == null || projectList.size() <= 0;
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);
        mainLayout.setVisibility(empty ? View.GONE : View.VISIBLE);
//        Util.showToast(getActivity(), "Project Count :: " + projectList.size());
        if (!empty) {
            projectItem = projectList.get(0);
        }

        if (pageList == null)
            pageList = new ArrayList<>();
        pageList.clear();
        pageList.addAll(db.getPageItems());

        previewAdapter.notifyDataSetChanged();

        if (pageAdapter != null)
            pageAdapter.notifyDataSetChanged();
        boolean show = pageList != null && pageList.size() > 0;
        editPageButton.setVisibility(show ? View.VISIBLE : View.GONE);
        newPageButton.setVisibility(show ? View.VISIBLE : View.GONE);
        previewImage.setVisibility(show ? View.GONE : View.VISIBLE);
        guideLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        publishButton.setVisibility(show ? View.VISIBLE : View.GONE);

        if (show) {
            int index = pageAdapter.getSelectedIndex();
            viewPager.setCurrentItem(index);
            editModeButton.setVisibility(pageList.size() > 1 ? View.VISIBLE : View.GONE);
        }
    }

    private boolean checkUpload() {
        if (pageList == null || pageList.size() < 2) {
            Util.showToast(getActivity(), r.s(R.string.message_need_add_page));
            return false;
        }

        if (pageList.get(0) != null && TextUtils.isEmpty(pageList.get(0).getPageFileUrl())) {
            Util.showToast(getActivity(), r.s(R.string.message_need_add_cover));
            return false;
        }

        return true;
    }

    private void setEditMode(boolean edit) {
        pageAdapter.setDeleteMode(edit);
        editModeButton.setVisibility(edit ? View.GONE : View.VISIBLE);
        completeButton.setVisibility(edit ? View.VISIBLE : View.GONE);
        guideText.setText(r.s(edit ? R.string.title_edit_mode : R.string.title_edit_page_sel));
    }

    private void movePlaylistItem(int oldPos, int newPos) {
        if (newPos == 0) {
            showToast(r.s(R.string.message_can_not_move));
            return;
        }
        PageItem item = pageList.get(oldPos);
        pageList.remove(oldPos);
        pageList.add(newPos, item);

        pageAdapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < pageList.size(); i++) {
            item = pageList.get(i);
            pageList.get(i).setIndex(i);
            item.setIndex(i);
            db.insertOrUpdatePageItem(item);
        }

        pageLv.postDelayed(() -> pageAdapter.notifyDataSetChanged(), 300);
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                movePlaylistItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return pageAdapter.isDeleteMode();
            }
        };

        return simpleCallback;
    }

    public void showToast(String message) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_toast,
                (ViewGroup) getActivity().findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text_message);
        text.setText(message);

        if (toast != null)
            toast.cancel();
        toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static interface PhotoBookFragmentListener {
        public void onOpenLibrary();
    }

    class PublishPhotoBook extends AsyncTask<String, Void, ArrayList<String>> {
        String title, author, publisher;

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            title = strings[0];
            author = strings[1];
            publisher = strings[2];
            String strFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + projectItem.getProjectId();
            File folder = new File(strFolderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            ArrayList<String> files = new ArrayList<>();
            for (int i = 0; i < pageList.size(); i++) {
                files.add(pageList.get(i).getPageFileUrl());
            }
//            for (PageItem item : pageList) {
//                FileOutputStream fos;
//                String strFilePath = strFolderPath + "/" + System.currentTimeMillis() + ".JPEG";
//                File fileCacheItem = new File(strFilePath);
//
//                try {
//                    fos = new FileOutputStream(fileCacheItem);
//                    Bitmap bmp = ImageUtil.getImage(item.getThumbnail());
//                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//
//                strFilePath = "file:///" + strFilePath;
//                getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(strFilePath)));
//
//                files.add(fileCacheItem.getAbsolutePath());
//            }
            return files;
        }

        @Override
        protected void onPostExecute(ArrayList<String> paths) {
            String cover = paths.get(0);
            paths.remove(0);
            api.publishPhotoBook(cover, paths, title, author, publisher);
        }
    }
}
