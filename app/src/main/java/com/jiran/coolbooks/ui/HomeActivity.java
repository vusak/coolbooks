package com.jiran.coolbooks.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.ui.dialog.MessageDialog;
import com.jiran.coolbooks.ui.fragment.BrowserFragment;
import com.jiran.coolbooks.ui.fragment.PhotoBookFragment;
import com.jiran.coolbooks.ui.fragment.SettingFragment;

public class HomeActivity extends BaseActivity implements View.OnClickListener, BrowserFragment.BrowserFragmentListener, PhotoBookFragment.PhotoBookFragmentListener {
    public static final int MENU_HOME = R.id.layout_home;
    public static final int MENU_SCHOOL = R.id.layout_school;
    public static final int MENU_PHOTO_BOOK = R.id.layout_photobook;
    public static final int MENU_LIBRARY = R.id.layout_library;
    public static final int MENU_SETTING = R.id.layout_setting;

    public static final int FRAGMENT_COOLBOOKS = 0;
    public static final int FRAGMENT_SCHOOL = 1;
    public static final int FRAGMENT_PHOTOBOOK = 2;
    public static final int FRAGMENT_LIBRARY = 3;
    public static final int FRAGMENT_SETTING = 4;

    LinearLayout homeLayout, schoolLayout, photoBookLayout, libraryLayout, settingLayout;
    ImageView homeImage, schoolImage, photoBookImage, libraryImage, settingImage;
    TextView homeText, schoolText, photoBookText, libraryText, settingText;

    //    ArrayList<BrowserFragment> browserFragments;
    BrowserFragment currentBrowserFragment;
    PhotoBookFragment photoBookFragment;
    SettingFragment settingFragment;

    LinearLayout browserFLayout;
    FrameLayout photoBookFLayout, settingFLayout;

    //    int current_fragment = FRAGMENT_COOLBOOKS;
    String current_url = Common.URL_COOLBOOKS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home);

        initView();
        setMenu(MENU_HOME);
    }

    @Override
    public void onBackPressed() {
        if (isBrowserVisibility()) {
            if (!currentBrowserFragment.onPrevButtonClicked()) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_exit));
                dlg.setPositiveListener((dialog, tag) -> finish());
                dlg.setPositiveLabel(r.s(R.string.quit));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                sdf(dlg);
            }
        } else {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setMessage(r.s(R.string.message_exit));
            dlg.setPositiveListener((dialog, tag) -> finish());
            dlg.setPositiveLabel(r.s(R.string.quit));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            sdf(dlg);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isBrowserVisibility() && currentBrowserFragment != null)
            currentBrowserFragment.setUserVisibleHint(true);

        if (isPhotobookVisibility() && photoBookFragment != null)
            photoBookFragment.setUserVisibleHint(true);
    }

    private void initView() {
        homeLayout = fv(R.id.layout_home);
        homeLayout.setOnClickListener(this);
        homeText = fv(R.id.text_home);
        homeImage = fv(R.id.image_home);

        schoolLayout = fv(R.id.layout_school);
        schoolLayout.setOnClickListener(this);
        schoolText = fv(R.id.text_school);
        schoolImage = fv(R.id.image_school);

        photoBookLayout = fv(R.id.layout_photobook);
        photoBookLayout.setOnClickListener(this);
        photoBookText = fv(R.id.text_photobook);
        photoBookImage = fv(R.id.image_photobook);
        libraryLayout = fv(R.id.layout_library);
        libraryLayout.setOnClickListener(this);
        libraryText = fv(R.id.text_library);
        libraryImage = fv(R.id.image_library);

        settingLayout = fv(R.id.layout_setting);
        settingLayout.setOnClickListener(this);
        settingText = fv(R.id.text_setting);
        settingImage = fv(R.id.image_setting);

        browserFLayout = fv(R.id.layout_fragment_browser);
        photoBookFLayout = fv(R.id.layout_fragment_photobook);
        settingFLayout = fv(R.id.layout_fragment_setting);

        initFragment();
    }

    private void initFragment() {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        currentBrowserFragment = new BrowserFragment();
        currentBrowserFragment.setListener(this);

        fragmentTransaction.replace(R.id.layout_fragment_browser, currentBrowserFragment).commit();

        photoBookFragment = new PhotoBookFragment();
        photoBookFragment.setListener(this);

        android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.layout_fragment_photobook, photoBookFragment).commit();

        settingFragment = new SettingFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction1.replace(R.id.layout_fragment_setting, settingFragment).commit();
    }

    private boolean isSettingVisibility() {
        return settingFLayout.getVisibility() == View.VISIBLE;
    }

    private boolean isPhotobookVisibility() {
        return photoBookFLayout.getVisibility() == View.VISIBLE;
    }

    private boolean isBrowserVisibility() {
        return browserFLayout.getVisibility() == View.VISIBLE;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case MENU_HOME:
                current_url = Common.URL_COOLBOOKS;
                break;
            case MENU_SCHOOL:
                current_url = Common.URL_COOLBOOKS_SCHOOL;
                break;
            case MENU_PHOTO_BOOK:
                current_url = Common.URL_COOLBOOKS_PHOTOBOOK;
                break;
            case MENU_LIBRARY:
                current_url = Common.URL_COOLBOOKS_BOOKSHELF;
                break;
            case MENU_SETTING:
                break;
        }
        setMenu(v.getId());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onOpenPhotoBook() {
        browserFLayout.setVisibility(View.GONE);
        photoBookFLayout.setVisibility(View.VISIBLE);
//        Intent intent = new Intent(this, PhotoBookFragment.class);
//        startActivityForResult(intent, 10001);
    }

    @Override
    public void onOpenLibrary() {
        current_url = Common.URL_COOLBOOKS_BOOKSHELF;
        setMenu(MENU_LIBRARY);
    }

    private void selectedFragment() {
        if (currentBrowserFragment != null) {
            currentBrowserFragment.clearHistory();
            currentBrowserFragment.goUrl(current_url);
        }
//        currentBrowserFragment = browserFragments.get(current_fragment);
//        BrowserItem item = currentBrowserFragment.getBrowserItem();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.layout_fragment_browser, currentBrowserFragment, item.getBrowserId());
//        transaction.commit();
    }

    private void setMenu(int id) {
        browserFLayout.setVisibility(id != MENU_SETTING ? View.VISIBLE : View.GONE);
        settingFLayout.setVisibility(id == MENU_SETTING ? View.VISIBLE : View.GONE);
        photoBookFLayout.setVisibility(View.GONE);
        homeText.setTextColor(ContextCompat.getColor(this, id == MENU_HOME ? R.color.blue_a100 : R.color.gray4_a100));
        homeImage.setImageResource(id == MENU_HOME ? R.drawable.ic_home_blue_24 : R.drawable.ic_home_gray_24);
        schoolText.setTextColor(ContextCompat.getColor(this, id == MENU_SCHOOL ? R.color.blue_a100 : R.color.gray4_a100));
        schoolImage.setImageResource(id == MENU_SCHOOL ? R.drawable.ic_school_blue_24 : R.drawable.ic_school_gray_24);
        photoBookText.setTextColor(ContextCompat.getColor(this, id == MENU_PHOTO_BOOK ? R.color.blue_a100 : R.color.gray4_a100));
        photoBookImage.setImageResource(id == MENU_PHOTO_BOOK ? R.drawable.ic_photo_album_blue_24 : R.drawable.ic_photo_album_gray_24);
        libraryText.setTextColor(ContextCompat.getColor(this, id == MENU_LIBRARY ? R.color.blue_a100 : R.color.gray4_a100));
        libraryImage.setImageResource(id == MENU_LIBRARY ? R.drawable.ic_book_blue_24 : R.drawable.ic_book_gray_24);
        settingText.setTextColor(ContextCompat.getColor(this, id == MENU_SETTING ? R.color.blue_a100 : R.color.gray4_a100));
        settingImage.setImageResource(id == MENU_SETTING ? R.drawable.ic_setting_blue_24 : R.drawable.ic_setting_gray_24);

        if (id != MENU_SETTING)
            selectedFragment();

    }


}
