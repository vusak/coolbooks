package com.jiran.coolbooks.ui.fragment;


import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseFragment;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.common.LogUtil;

import java.net.URISyntaxException;

public class BrowserFragment extends BaseFragment {

    public static final String INTENT_URI_START = "intent:";
    public static final String INTENT_FALLBACK_URL = "browser_fallback_url";
    public static final String URI_SCHEME_MARKET = "market://details?id=";
    private static BrowserFragment fragment;
    private final Handler handler = new Handler();
    public WebView webView;
    ProgressBar loadingProgress;
    float initialY = -1;
    String checkUrl = "";
    //    BrowserItem item;
    String mainUrl = Common.URL_COOLBOOKS;
    LinearLayout rootLayout;
    LinearLayout mainTopLayout;
    CookieManager cookieManager;
    boolean isClearHistory = false;
    private RelativeLayout childLayout;
    private RelativeLayout browserLayout;
    private ImageButton mainCloseButton;
    private TextView titleText;
    private BrowserFragmentListener listener;

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();

//        if (webView != null) {
//            if ((webView.copyBackForwardList() == null || (webView.copyBackForwardList().getSize() <= 0) && (item != null && item.getBrowserHistory() != null))) {
//                if (webView.getOriginalUrl() != null && webView.getOriginalUrl().equalsIgnoreCase(item.getBrowserUrl()))
//                    return;
//                if (!item.isBrowserPrivacy()) {
//                    goUrl(item.getBrowserUrl());
//                } else {
//                    byte[] bundleBytes = item.getBrowserHistory();
//                    final Parcel parcel = Parcel.obtain();
//                    parcel.unmarshall(bundleBytes, 0, bundleBytes.length);
//                    parcel.setDataPosition(0);
//                    Bundle bundle = (Bundle) parcel.readBundle();
//                    parcel.recycle();
//
//                    webView.restoreState(bundle);
//                }
//            }
//
//            if (item != null && webView != null && webView.getUrl() == null)
//                goUrl(Common.URL_COOLBOOKS);
//        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser && webView != null) {
            webView.loadUrl(mainUrl);

            if (webView != null) {
                webView.resumeTimers();
                webView.onResume();
                webView.reload();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_browser;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }


    }

    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
            webView.destroy();
        }

        super.onDestroy();
    }

    public boolean onPrevButtonClicked() {
        if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
            WebView view = (WebView) browserLayout.getChildAt(0);
            if (view != null && view.canGoBack())
                view.goBack();
            else
                closeChild();
            return true;
        } else if (webView != null && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return false;
    }

    public void onNextButtonClicked() {
        if (webView != null)
            webView.goForward();
    }

//    public BrowserItem getBrowserItem() {
//        return this.item;
//    }
//
//    public void setBrowserItem(BrowserItem item) {
//        if (item == null)
//            return;
//
//        this.item = item;
//
//    }

    public void setListener(BrowserFragmentListener listener) {
        this.listener = listener;
    }

    public void clearHistory() {
        if (webView != null)
            webView.clearHistory();
    }

//    public void saveBrowserItem() {
//        Bundle bundle = new Bundle();
//        webView.saveState(bundle);
//        final Parcel parcel = Parcel.obtain();
//        bundle.writeToParcel(parcel, 0);
//        byte[] bundleBytes = parcel.marshall();
//        parcel.recycle();
//
//        if (bundleBytes != null)
//            item.setBrowserHistory(bundleBytes);
//        item.setBrowserPrivacy(true);
//        item.setBrowserAt(System.currentTimeMillis());
//        db.insertOrUpdateBrowserItem(item);
//    }

    public void goUrl(final String url) {
        mainUrl = url;
        if (webView != null)
            webView.post(() -> {
                webView.onResume();
                if (childLayout != null && childLayout.getVisibility() == View.VISIBLE)
                    closeChild();
                isClearHistory = true;
                webView.loadUrl(url);
            });

    }

    private void initView() {
        // Enable remote debugging via chrome://inspect
        if (LogUtil.isDebugMode() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        rootLayout = fv(R.id.layout_root);
        browserLayout = fv(R.id.mainBrowserLayout);
        childLayout = fv(R.id.mainAdChildLayout);
        titleText = fv(R.id.mainTitleText);
        mainTopLayout = fv(R.id.mainLinearLayout);
        mainCloseButton = fv(R.id.mainCloseButton);
        mainCloseButton.setOnClickListener(v -> closeChild());

        webView = fv(R.id.webview);

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(false);

        // Use WideViewport and Zoom out if there is no viewport defined
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        // Enable Javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        cookieManager = CookieManager.getInstance();
        cookieManager.acceptCookie();
        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }

        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);

        webView.getSettings().setSavePassword(true);
        webView.getSettings().setSaveFormData(true);

        webView.getSettings().setSupportMultipleWindows(true);

        // scroll
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollContainer(false);

        webView.setWebViewClient(new MyWebViewClient());
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android_CoolBooks");
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                loadingProgress.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onCloseWindow(WebView window) {
                super.onCloseWindow(window);
                closeChild();
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView.HitTestResult result = view.getHitTestResult();
                String url = result.getExtra();
                if (TextUtils.isEmpty(url)) {
                    Message href = view.getHandler().obtainMessage();
                    view.requestFocusNodeHref(href);

                    url = href.getData().getString("url");
                    if (TextUtils.isEmpty(url)) {
                        openChild(resultMsg);
                        return true;
                    }
                }
                switch (result.getType()) {
                    case WebView.HitTestResult.EMAIL_TYPE:
                        openEmail(url);
                        return false;
                }
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
                        closeChild();
                    }
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                }
                // 구글 마켓 이동 doFallback
                else if (url.toLowerCase().startsWith(INTENT_URI_START)) {
                    Intent parsedIntent = null;
                    try {
                        parsedIntent = Intent.parseUri(url, 0);
                        startActivity(parsedIntent);
                    } catch (ActivityNotFoundException | URISyntaxException e) {
                        return doFallback(view, parsedIntent);
                    }
                } else {
                    openChild(resultMsg);
                }
                return true;
            }
        });

        loadingProgress = fv(R.id.progress_url);
    }

    public void stopWebViewLoading() {
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    // 구글 마켓 이동
    private boolean doFallback(WebView view, Intent parsedIntent) {
        if (parsedIntent == null) {
            return false;
        }

        String fallbackUrl = parsedIntent.getStringExtra(INTENT_FALLBACK_URL);
        if (fallbackUrl != null) {
            view.loadUrl(fallbackUrl);
            return true;
        }

        String packageName = parsedIntent.getPackage();
        if (packageName != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URI_SCHEME_MARKET + packageName)));
            return true;
        }
        return false;
    }

    private void closeChild() {
        int colorStart = r.c(getActivity(), R.color.black_a30);
        int colorEnd = Color.TRANSPARENT;

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(100);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();

        Animation slideClose = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_close);
        childLayout.startAnimation(slideClose);
        slideClose.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                titleText.setText("");
                childLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void openChild(Message resultMsg) {
        // remove any current child views
        browserLayout.removeAllViews();
        // make the child web view's layout visible
        childLayout.setVisibility(View.VISIBLE);

        // now create a new web view
        WebView newView = new WebView(getActivity());
        newView.setScrollContainer(false);

        WebSettings settings = newView.getSettings();
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportMultipleWindows(true);
        settings.setUseWideViewPort(false);

        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            cookieManager.setAcceptThirdPartyCookies(newView, true);
        }
        newView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                // once the view has loaded, display its title
                titleText.setText(view.getTitle());
            }
        });
        newView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                openChild(resultMsg);
                return true;
            }

            @Override
            public void onCloseWindow(WebView window) {
                super.onCloseWindow(window);
                closeChild();
            }
        });
        // add the new web view to the layout
        newView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        browserLayout.addView(newView);
        // tell the transport about the new view
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(newView);
        resultMsg.sendToTarget();

        // let's be cool and slide the new web view up into view
        Animation slideOpen = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_open);
        childLayout.startAnimation(slideOpen);

        int colorStart = Color.TRANSPARENT;
        int colorEnd = r.c(getActivity(), R.color.black_a30);

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(4000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();
    }

    private void openEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + email));
        intent.putExtra(Intent.EXTRA_EMAIL, email);

        startActivity(intent);
    }

    public static interface BrowserFragmentListener {
        public void onOpenPhotoBook();
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            resend.sendToTarget();
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            if (url.equals("about:blank")) {
            }
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (isClearHistory) {
                isClearHistory = false;
                view.clearHistory();
            }
            loadingProgress.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            compareText = url;
            loadingProgress.setVisibility(View.INVISIBLE);
            LogUtil.log("cecil onpageFinished :: " + url);

            if (isClearHistory) {
                view.clearHistory();
            }

            if (url.equals("about:blank")) {
                return;
            }

//            item.setBrowserUrl(url);
//            sp.setBrowserUrl(url);

//            addHistoryList(currentSite);

            super.onPageFinished(view, url);
        }

        @Override
        public void onLoadResource(WebView view, final String url) {


            super.onLoadResource(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url == null || url.equals("about:blank"))
                return true;

            if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
                    closeChild();
                }
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
            // 구글 마켓 이동 doFallback
            else if (url.toLowerCase().startsWith(INTENT_URI_START)) {
                Intent parsedIntent = null;
                try {
                    parsedIntent = Intent.parseUri(url, 0);
                    startActivity(parsedIntent);
                } catch (ActivityNotFoundException | URISyntaxException e) {
                    return doFallback(view, parsedIntent);
                }
            } else if (url.startsWith("mailto:")) {
                return true;
            } else {
                view.loadUrl(url);
                return false;
            }
            return true;
        }
    }

    private class MyJavascriptInterface {
        @JavascriptInterface
        public void loadPhotobook(String user_id) {
            sp.setLoginedUserId(user_id.toLowerCase());
            handler.post(() -> {
                webView.clearHistory();
                if (listener != null)
                    listener.onOpenPhotoBook();
            });
        }
    }

}

