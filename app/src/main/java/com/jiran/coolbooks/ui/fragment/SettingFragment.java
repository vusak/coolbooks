package com.jiran.coolbooks.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseFragment;
import com.jiran.coolbooks.ui.PrivacyActivity;


public class SettingFragment extends BaseFragment implements OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static final String TAG = SettingFragment.class.getSimpleName();

    TextView versionText;
    LinearLayout privacyLayout, serviceCenterLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        versionText = fv(R.id.text_version);

        privacyLayout = fv(R.id.layout_privacy_policy);
        privacyLayout.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), PrivacyActivity.class);
            startActivity(intent);
        });

        serviceCenterLayout = fv(R.id.layout_service_center);
        serviceCenterLayout.setOnClickListener(v -> {
            String tel = "tel:0263256300";
            startActivity(new Intent("android.intent.action.DIAL", Uri.parse(tel)));
        });

        setVersionInfo();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

        }
    }

    private void setVersionInfo() {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            versionText.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
