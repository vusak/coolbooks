package com.jiran.coolbooks.ui;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;
import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.data.BrowserItem;

public class IntroActivity extends BaseActivity {
    public static final int MENU_HOME = R.id.layout_home;
    public static final int MENU_SCHOOL = R.id.layout_school;
    public static final int MENU_PHOTO_BOOK = R.id.layout_photobook;
    public static final int MENU_LIBRARY = R.id.layout_library;
    public static final int MENU_SETTING = R.id.layout_setting;


    LottieAnimationView animationView;

    //    boolean finish = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);

        animationView = fv(R.id.lottie_intro);
        animationView.setRepeatCount(10);
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
//                if (finish) {
                Intent intent = new Intent(IntroActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
//                }
            }
        });

//        setDB();
    }

    private void setDB() {
        if (db.getBrowserItem("" + MENU_HOME) == null) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId("" + MENU_HOME);
            item.setBrowserTitle(r.s(R.string.menu_coolbooks));
            item.setBrowserUrl(Common.URL_COOLBOOKS);
            item.setBrowserPrivacy(false);
            item.setBrowserOrder(0);
            item.setBrowserAt(System.currentTimeMillis());
            db.insertOrUpdateBrowserItem(item);
        }

        if (db.getBrowserItem("" + MENU_SCHOOL) == null) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId("" + MENU_SCHOOL);
            item.setBrowserTitle(r.s(R.string.menu_school));
            item.setBrowserUrl(Common.URL_COOLBOOKS_SCHOOL);
            item.setBrowserPrivacy(false);
            item.setBrowserOrder(1);
            item.setBrowserAt(System.currentTimeMillis());
            db.insertOrUpdateBrowserItem(item);
        }

        if (db.getBrowserItem("" + MENU_PHOTO_BOOK) == null) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId("" + MENU_PHOTO_BOOK);
            item.setBrowserTitle(r.s(R.string.menu_photo_book));
            item.setBrowserUrl(Common.URL_COOLBOOKS_PHOTOBOOK);
            item.setBrowserPrivacy(false);
            item.setBrowserOrder(2);
            item.setBrowserAt(System.currentTimeMillis());
            db.insertOrUpdateBrowserItem(item);
        }

        if (db.getBrowserItem("" + MENU_LIBRARY) == null) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId("" + MENU_LIBRARY);
            item.setBrowserTitle(r.s(R.string.menu_library));
            item.setBrowserUrl(Common.URL_COOLBOOKS_BOOKSHELF);
            item.setBrowserPrivacy(false);
            item.setBrowserOrder(3);
            item.setBrowserAt(System.currentTimeMillis());
            db.insertOrUpdateBrowserItem(item);
        }

//        finish = true;
    }
}
