package com.jiran.coolbooks.ui;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.adapter.CollageRAdapter;
import com.jiran.coolbooks.base.BaseActivity;
import com.jiran.coolbooks.base.CollageViewGroup;
import com.jiran.coolbooks.common.Common;
import com.jiran.coolbooks.common.FileUtil;
import com.jiran.coolbooks.common.LogUtil;
import com.jiran.coolbooks.common.Util;
import com.jiran.coolbooks.common.collage.CollageFactory;
import com.jiran.coolbooks.common.collage.SimpleCollageGenerator;
import com.jiran.coolbooks.data.Collage;
import com.jiran.coolbooks.data.CollageFillData;
import com.jiran.coolbooks.data.CollageRegion;
import com.jiran.coolbooks.data.CollageRegionData;
import com.jiran.coolbooks.data.PageItem;
import com.jiran.coolbooks.data.ProjectItem;
import com.jiran.coolbooks.ui.dialog.MessageDialog;
import com.jiran.coolbooks.ui.dialog.PhotoBookStartDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

public class CollageActivity extends BaseActivity {
    public static final int COLLAGE_COVER_TOP = 8;
    public static final int COLLAGE_COVER_BOTTOM = 9;
    private static final CollageFactory COLLAGE_FACTORY = new SimpleCollageGenerator();
    private static final int COLLAGE_COVER = 0;
    private static final int SAVE_MODE_NORMAL = 0;
    private static final int SAVE_MODE_CLOSE = 1;
    private static final int SAVE_MODE_NEW = 2;
    private static final int SAVE_MODE_COVER = 3;

    Uri uri;
    ProjectItem projectItem;
    LinearLayout coverTitleLayout, coverLayout, topCoverLayout, bottomCoverLayout;
    TextView topText, bottomText;
    ImageView topImage, bottomImage;
    TextView titleCoverText, authorCoverText;
    private CollageFillData mCollageFillData;
    private CollageRegion mSelectedCollageRegion;
    private CollageViewGroup mCollageViewGroup;
    //    private Button previewButton, saveButton;
    private ImageView previewImage;
    private LinearLayout collageLayout;
    private TextView previewText, pageText;
    private ProgressBar progress;
    private String pageId, projectId;
    private PageItem pageItem;
    private ArrayList<PageItem> pageList;
    //    private ArrayList<CollageRegionData> regionDataItems;
    private Hashtable<Integer, CollageRegionData> regionDataItems;
    private ImageButton backButton, savePageButton, newPageButton;
    private CollageRAdapter collageAdapter;
    private RecyclerView collageLv;
    private LinearLayoutManager collageManager;
    private ArrayList<Collage> collageList;
    private int saveMode = SAVE_MODE_NORMAL;

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);

        projectId = getIntent().getStringExtra(Common.INTENT_PROJECT_ID);
        pageId = getIntent().getStringExtra(Common.INTENT_PAGE_ID);

        pageList = new ArrayList<>();
        pageList.addAll(db.getPageItemByProject(projectId));

        projectItem = db.getProjectItem(projectId);
        pageItem = db.getPageItem(pageId);
        if (TextUtils.isEmpty(pageId)) {
            pageItem = new PageItem();
            pageItem.setIndex(pageList.size());
            pageItem.setProjectId(projectId);
            pageItem.setCollageIndex(isCover() ? COLLAGE_COVER_TOP : 0);

            pageId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        }

        mCollageFillData = new CollageFillData(COLLAGE_FACTORY.getCollage(pageItem.getCollageIndex()));
        initView();

        String[] projection = new String[]{"DISTINCT " + MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME};
        Cursor cur = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
        StringBuffer list = new StringBuffer();
        while (cur.moveToNext()) {
            list.append(cur.getString((cur.getColumnIndex(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME))) + "\n");
        }
    }

    @Override
    public void onBackPressed() {
//        if (previewImage != null && previewImage.getVisibility() == View.VISIBLE) {
//            previewImage.setVisibility(View.GONE);
//            saveButton.setVisibility(View.GONE);
//            previewButton.setVisibility(View.VISIBLE);
//            previewText.setVisibility(View.GONE);
//        }
//        else {
        closePhotoBook();
//        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 10001) {
                    // Get the url from data
                    uri = data.getParcelableExtra("file");

                    if (uri == null) {
                        uri = data.getData();
                    }
                }
                CollageRegionData collageRegionData = new CollageRegionData(uri.toString());
                collageRegionData.setRegionPageId(pageId);
                collageRegionData.setRegionId(mSelectedCollageRegion.getId());
//                db.insertOrUpdateCollageRegionData(collageRegionData);
                mCollageFillData.setRegionData(mSelectedCollageRegion, collageRegionData);
                mCollageViewGroup.invalidateRegionData(false);

                regionDataItems.put(mSelectedCollageRegion.getId(), collageRegionData);
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    private void initView() {
        mCollageViewGroup = fv(R.id.collage);
        mCollageViewGroup.setCollage(mCollageFillData);
//        mCollageViewGroup.setBackgroundColor(pageItem.getFrameColor());
        mCollageViewGroup.setRegionClickListener(new CollageViewGroup.RegionClickListener() {
            @Override
            public void onRegionClicked(CollageRegion collageRegion) {
                mSelectedCollageRegion = collageRegion;

                openGallery();
            }

            @Override
            public void onRegionDataInvalidated() {
//                mCollageViewGroup.postDelayed(() -> new UpdatePageItem().execute(), 100);

            }

            @Override
            public void onRegionDataInvalidated(int id, CollageRegionData data) {
//                CollageRegionData dbData = db.getCollageRegionData(id, pageId);
//                if (dbData != null) {
//                    dbData.setImageLeft(data.getImageLeft());
//                    dbData.setImageTop(data.getImageTop());
//                    dbData.setImageScale(data.getImageScale());
//                    db.insertOrUpdateCollageRegionData(dbData);
//                }
            }
        });

        collageLayout = fv(R.id.layout_collage);
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            closePhotoBook();
        });

        newPageButton = fv(R.id.button_new_page);
        newPageButton.setOnClickListener(v -> {
            if (regionDataItems == null || regionDataItems.size() <= 0) { // mCollageFillData.getRegionsCount()) {
                Util.showToast(CollageActivity.this, r.s(R.string.message_empty_collage));
            } else {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_save_page));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setNegativeListener((dialog, tag) -> newPage());
                dlg.setPositiveLabel(r.s(R.string.save));
                dlg.setPositiveListener((dialog, tag) -> {
                    saveMode = SAVE_MODE_NEW;
                    saveCurrentPage();


                });
                sdf(dlg);
            }
        });

        savePageButton = fv(R.id.button_save_page);
        savePageButton.setOnClickListener(v -> {
            if (regionDataItems == null || regionDataItems.size() <= 0) { //mCollageFillData.getRegionsCount()) {
                Util.showToast(CollageActivity.this, r.s(R.string.message_empty_collage));
            } else {
                saveMode = SAVE_MODE_CLOSE;
                saveCurrentPage();
            }
        });

        pageText = fv(R.id.text_page);
        pageText.setText(pageItem.getIndex() == 0 ? r.s(R.string.cover) : pageItem.getIndex() + r.s(R.string.page));

        previewImage = fv(R.id.image_preview);
        previewImage.setVisibility(View.GONE);

        progress = fv(R.id.progress);
        previewText = fv(R.id.text_preview);

        collageList = new ArrayList<>();
        for (int i = 0; i < COLLAGE_FACTORY.getCollageCount(); i++) {
            collageList.add(COLLAGE_FACTORY.getCollage(i));
        }

        collageLv = fv(R.id.lv_collage);
        collageManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        collageLv.setLayoutManager(collageManager);
        this.collageAdapter = new CollageRAdapter(this, R.layout.item_collage, collageList, new CollageRAdapter.CollageRAdapterListener() {
            @Override
            public void OnItemClick(int position, Collage item) {
                collageAdapter.setSelectedIndex(position);
//                selectedCollage = position;

                mCollageFillData = new CollageFillData(COLLAGE_FACTORY.getCollage(position));
                mCollageViewGroup.setCollage(mCollageFillData);
                refreshRegionData();
            }

            @Override
            public void OnItemLongClick(int position, Collage item) {

            }
        });
//        collageAdapter.setSelectedIndex(pageItem.getCollageIndex());
        collageAdapter.setSelectedIndex(pageItem.getCollageIndex());
        collageLv.setAdapter(this.collageAdapter);
        collageLv.setVisibility(isCover() ? View.GONE : View.VISIBLE);

        coverLayout = fv(R.id.layout_cover);
        coverLayout.setVisibility(isCover() ? View.VISIBLE : View.GONE);
        coverTitleLayout = fv(R.id.layout_cover_title);
        coverTitleLayout.setOnClickListener(v -> {
            PhotoBookStartDialog dlg = PhotoBookStartDialog.newInstance(PhotoBookStartDialog.TAG, projectItem.getTitle(), projectItem.getAuthor(), projectItem.getPublisher());
            dlg.setListener(new PhotoBookStartDialog.UploadingDialogListener() {
                @Override
                public void onStart(String title, String author, String publisher) {
                    projectItem.setTitle(title);
                    projectItem.setAuthor(author);
                    projectItem.setPublisher(publisher);

                    titleCoverText.setText(title);
                    authorCoverText.setText(author);
                }

                @Override
                public void onCancel() {

                }
            });
            sdf(dlg);
        });
        coverTitleLayout.setVisibility(isCover() ? View.VISIBLE : View.GONE);

        topCoverLayout = fv(R.id.layout_top);
        topCoverLayout.setOnClickListener(v -> {
            setCoverLayout(COLLAGE_COVER_TOP);
        });
        topText = fv(R.id.text_top);
        topImage = fv(R.id.image_top);

        bottomCoverLayout = fv(R.id.layout_bottom);
        bottomCoverLayout.setOnClickListener(v -> {
            setCoverLayout(COLLAGE_COVER_BOTTOM);
        });
        bottomText = fv(R.id.text_bottom);
        bottomImage = fv(R.id.image_bottom);

        titleCoverText = fv(R.id.text_cover_title);
        if (projectItem != null && !TextUtils.isEmpty(projectItem.getTitle()))
            titleCoverText.setText(projectItem.getTitle());
        authorCoverText = fv(R.id.text_cover_author);
        if (projectItem != null && !TextUtils.isEmpty(projectItem.getAuthor()))
            authorCoverText.setText(projectItem.getAuthor());
        initRegionData();
    }

    private void setCoverLayout(int index) {

        pageItem.setCollageIndex(index);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) Util.convertDpToPixel(200f, CollageActivity.this));
        params.gravity = isTop() ? Gravity.TOP : Gravity.BOTTOM;
        params.leftMargin = (int) Util.convertDpToPixel(16f, CollageActivity.this);
        params.rightMargin = (int) Util.convertDpToPixel(16f, CollageActivity.this);
        coverTitleLayout.setLayoutParams(params);

        topText.setTextColor(ContextCompat.getColor(this, isTop() ? R.color.blue_a100 : R.color.gray4_a100));
        bottomText.setTextColor(ContextCompat.getColor(this, isBottom() ? R.color.blue_a100 : R.color.gray4_a100));
        topImage.setVisibility(isTop() ? View.VISIBLE : View.GONE);
        bottomImage.setVisibility(isBottom() ? View.VISIBLE : View.GONE);

        mCollageFillData = new CollageFillData(COLLAGE_FACTORY.getCollage(index));
        mCollageViewGroup.setCollage(mCollageFillData);
        refreshRegionData();
    }

    private void closePhotoBook() {
        if (regionDataItems == null || regionDataItems.size() <= 0) {
            Intent intent = new Intent();
            intent.putExtra(Common.INTENT_PAGE_INDEX, pageItem.getIndex());
            setResult(RESULT_OK, intent);
            finish();
        } else {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setMessage(r.s(R.string.message_save_page));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setNegativeListener((dialog, tag) -> {
                Intent intent = new Intent();
                intent.putExtra(Common.INTENT_PAGE_INDEX, pageItem.getIndex());
                setResult(RESULT_OK, intent);
                finish();
            });
            dlg.setPositiveLabel(r.s(R.string.save));
            dlg.setPositiveListener((dialog, tag) -> {
                saveMode = SAVE_MODE_CLOSE;
                saveCurrentPage();

            });
            sdf(dlg);
        }
    }

    private void initRegionData() {
        if (isCover()) {
            coverTitleLayout.post(new Runnable() {
                @Override
                public void run() {
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) Util.convertDpToPixel(200f, CollageActivity.this));
                    params.gravity = isTop() ? Gravity.TOP : Gravity.BOTTOM;
                    params.leftMargin = (int) Util.convertDpToPixel(16f, CollageActivity.this);
                    params.rightMargin = (int) Util.convertDpToPixel(16f, CollageActivity.this);
                    coverTitleLayout.setLayoutParams(params);
                    topText.setTextColor(ContextCompat.getColor(CollageActivity.this, isTop() ? R.color.blue_a100 : R.color.gray4_a100));
                    bottomText.setTextColor(ContextCompat.getColor(CollageActivity.this, isBottom() ? R.color.blue_a100 : R.color.gray4_a100));
                    topImage.setVisibility(isTop() ? View.VISIBLE : View.GONE);
                    bottomImage.setVisibility(isBottom() ? View.VISIBLE : View.GONE);

                }
            });
        }

        ArrayList<CollageRegionData> list = new ArrayList<>();
        list.addAll(db.getCollageRegionDataByPage(pageId));

        if (regionDataItems == null)
            regionDataItems = new Hashtable<>();
        regionDataItems.clear();
        if (list != null && list.size() > 0) {
            for (CollageRegionData item : list) {
                regionDataItems.put(item.getRegionId(), item);
                if (item.getRegionId() < mCollageFillData.getRegionsCount()) {
                    CollageRegion region = mCollageFillData.getCollageRegions().get(item.getRegionId());
//                CollageRegion region = ((CollageItemView) (mCollageViewGroup.getChildAt(item.getRegionId()))).getCollageRegion();
                    if (region != null)
                        mCollageFillData.setRegionData(region, item);
                }
            }
            mCollageViewGroup.invalidateRegionData(false);
        }
    }

    private void refreshRegionData() {
        if (regionDataItems != null && regionDataItems.size() > 0) {
            for (int i = 0; i < regionDataItems.size(); i++) {
                CollageRegionData item = regionDataItems.get(i);
                if (item.getRegionId() < mCollageFillData.getRegionsCount()) {
                    CollageRegion region = mCollageFillData.getCollageRegions().get(item.getRegionId());
//                CollageRegion region = ((CollageItemView) (mCollageViewGroup.getChildAt(item.getRegionId()))).getCollageRegion();
                    if (region != null)
                        mCollageFillData.setRegionData(region, item);
                }
            }
            mCollageViewGroup.invalidateRegionData(false);
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 10001);
    }

    private File getFile(Uri uri) {
        String path = getRealPathFromURI(uri);
        File file = new File(path);

        return file;
    }

    @SuppressLint("newAPI")
    private String getRealPathFromURI(Uri contentUri) {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(getApplicationContext(), contentUri)) {
            if (isExternalStorageDocument(contentUri)) {
                final String docId = DocumentsContract.getDocumentId(contentUri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(contentUri)) {
                final String id = DocumentsContract.getDocumentId(contentUri);
                contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(contentUri)) {
                final String docId = DocumentsContract.getDocumentId(contentUri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(contentUri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(contentUri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(contentUri.getScheme())) {
            return contentUri.getPath();
        }
        return null;
//        if (contentUri.getPath().startsWith("/storage")) {
//            return contentUri.getPath();
//        }
//        String id = DocumentsContract.getDocumentId(contentUri).split(":")[1];
//        String[] columns = {MediaStore.Files.FileColumns.DATA};
//        String selection = MediaStore.Files.FileColumns._ID + " = " + id;
//        Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"), columns, selection, null, null);
//        try {
//            int columnIndex = cursor.getColumnIndex(columns[0]);
//            if (cursor.moveToFirst()) {
//                return cursor.getString(columnIndex);
//            }
//        } finally {
//            cursor.close();
//        }
//        return null;
    }

    public void saveCurrentPage() {
        progress.setVisibility(View.VISIBLE);
        mCollageViewGroup.invalidateRegionData(true);
        if (isCover()) {
            titleCoverText.setBackgroundResource(R.color.transparent);
            authorCoverText.setBackgroundResource(R.color.transparent);
        }
        collageLayout.postDelayed(() -> {
            View view = collageLayout;
            view.setDrawingCacheEnabled(false);
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache();
            Bitmap bmp = view.getDrawingCache();
            new SavePageThumbnail().execute(bmp);
        }, 100);
    }

    private void copyToCover() {
//        if (regionDataItems == null || regionDataItems.size() < mCollageFillData.getRegionsCount()) {
        if (regionDataItems == null || regionDataItems.size() <= 0) {
            Util.showToast(this, r.s(R.string.message_empty_collage));
            return;
        } else {
            PageItem cover = pageList.get(0);
            if (!TextUtils.isEmpty(cover.getPageFileUrl())) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_exist_cover));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.copy));
                dlg.setPositiveListener((dialog, tag) -> {
                    saveMode = SAVE_MODE_COVER;
                    saveCurrentPage();
                });
                sdf(dlg);
            } else {
                saveMode = SAVE_MODE_COVER;
                saveCurrentPage();
            }
        }
    }
//    public void saveView(View View) {
//        progress.setVisibility(View.VISIBLE);
//        View.buildDrawingCache();
//        Bitmap captureView = View.getDrawingCache();
//
//        FileOutputStream fos;
//
//        String strFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/TEST";
//        File folder = new File(strFolderPath);
//        if (!folder.exists()) {
//            folder.mkdirs();
//        }
//
//        String strFilePath = strFolderPath + "/" + System.currentTimeMillis() + ".png";
//        File fileCacheItem = new File(strFilePath);
//
//        try {
//            fos = new FileOutputStream(fileCacheItem);
//            captureView.compress(Bitmap.CompressFormat.PNG, 100, fos);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        previewImage.postDelayed(() -> finish(), 1500);
//
//
//        strFilePath = "file:///" + strFilePath;
//        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(strFilePath)));
//
//    }

    private void newPage() {
        pageList.clear();
        pageList.addAll(db.getPageItems());

        if (collageAdapter != null)
            collageAdapter.setSelectedIndex(0);
        regionDataItems.clear();

        pageItem = null;
        pageItem = new PageItem();
        pageItem.setIndex(pageList.size());
        pageItem.setProjectId(projectId);
        pageItem.setCollageIndex(isCover() ? COLLAGE_COVER_TOP : 0);

        pageText.setText(isCover() ? r.s(R.string.cover) : pageItem.getIndex() + r.s(R.string.page));
        coverTitleLayout.setVisibility(isCover() ? View.VISIBLE : View.GONE);
        coverLayout.setVisibility(isCover() ? View.VISIBLE : View.GONE);
        collageLv.setVisibility(isCover() ? View.GONE : View.VISIBLE);

        pageId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        mCollageFillData = new CollageFillData(COLLAGE_FACTORY.getCollage(0));
        mCollageViewGroup.setCollage(mCollageFillData);
        refreshRegionData();
    }

    private boolean isCover() {
        return pageItem != null && pageItem.getIndex() == COLLAGE_COVER;
    }

    private boolean isTop() {
        return pageItem != null && pageItem.getIndex() == 0 && pageItem.getCollageIndex() == COLLAGE_COVER_TOP;
    }

    private boolean isBottom() {
        return pageItem != null && pageItem.getIndex() == 0 && pageItem.getCollageIndex() == COLLAGE_COVER_BOTTOM;
    }

    class SavePageThumbnail extends AsyncTask<Bitmap, Void, String> {
        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            Bitmap bmp = bitmaps[0];

            FileOutputStream fos;

            String strFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + projectId;
            File folder = new File(strFolderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            String strFilePath = strFolderPath + "/." + pageId + ".JPEG";
            File fileCacheItem = new File(strFilePath);

            try {
                fos = new FileOutputStream(fileCacheItem);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(pageItem.getPageId())) {
                pageItem.setPageId(pageId);
            }
            pageItem.setProjectId(projectId);
            if (!isCover())
                pageItem.setCollageIndex(collageAdapter.getSelectedIndex());
            pageItem.setPageFileUrl(fileCacheItem.getAbsolutePath());
//            pageItem.setThumbnail(thumbnail);
            db.insertOrUpdatePageItem(pageItem);
            db.insertOrUpdateProjectItem(projectItem);
            for (int i = 0; i < mCollageFillData.getCollageRegions().size(); i++) {
                CollageRegionData data = mCollageFillData.getRegionData(mCollageFillData.getCollageRegions().get(i));
                if (data != null) {
                    data.setRegionPageId(pageItem.getPageId());
                    db.insertOrUpdateCollageRegionData(data);
                }
            }

            if (saveMode == SAVE_MODE_COVER) {
                PageItem coverPage = pageList.get(0);

                String coverFilePath = strFolderPath + "/" + coverPage.getPageId() + ".JPEG";
                File fileCoverCacheItem = new File(coverFilePath);

                try {
                    FileUtil.copyFile(fileCacheItem.getAbsolutePath(), fileCoverCacheItem.getAbsolutePath());
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }

                coverPage.setProjectId(projectId);
                coverPage.setCollageIndex(collageAdapter.getSelectedIndex());
                coverPage.setPageFileUrl(fileCoverCacheItem.getAbsolutePath());
                db.insertOrUpdatePageItem(coverPage);

                for (int i = 0; i < mCollageFillData.getCollageRegions().size(); i++) {
                    CollageRegionData data = mCollageFillData.getRegionData(mCollageFillData.getCollageRegions().get(i));
                    if (data != null) {
                        data.setRegionPageId(coverPage.getPageId());
                        db.insertOrUpdateCollageRegionData(data);
                    }
                }
            }
            return strFilePath;
        }

        @Override
        protected void onPostExecute(String strFilePath) {
//            strFilePath = "file:///" + strFilePath;
//            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(strFilePath)));
            progress.setVisibility(View.GONE);

            if (saveMode == SAVE_MODE_NEW) {
                newPage();
            } else if (saveMode == SAVE_MODE_CLOSE) {
                Intent intent = new Intent();
                intent.putExtra(Common.INTENT_PAGE_INDEX, pageItem.getIndex());
                setResult(RESULT_OK, intent);
                finish();
            } else if (saveMode == SAVE_MODE_COVER) {
//                PageItem coverPage = pageList.get(0);
//                for (int i = 0; i < mCollageFillData.getCollageRegions().size(); i++) {
//                    CollageRegionData data = mCollageFillData.getRegionData(mCollageFillData.getCollageRegions().get(i));
//                    data.setRegionPageId(coverPage.getPageId());
//                    db.insertOrUpdateCollageRegionData(data);
//                }
                Util.showToast(CollageActivity.this, r.s(R.string.message_copy_to_cover));
            }
        }
    }
}
