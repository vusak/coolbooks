package com.jiran.coolbooks.ui.dialog;


import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiran.coolbooks.R;
import com.jiran.coolbooks.base.BaseDialogFragment;

public class MessageDialog extends BaseDialogFragment {

    public static final String TAG = MessageDialog.class.getSimpleName();

    private static final String TAG_TITLE = "title";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_INFO = "info";
    private static final String TAG_POSITIVE_LABEL = "positive_label";
    private static final String TAG_NEGATIVE_LABEL = "negative_label";
    private static final String TAG_SHOW_PROGRESS = "show_progress";

    private TextView titleText, messageText, infoText;
    private View buttonLayout;
    private Button positiveButton, negativeButton;
    private LinearLayout bodyLayout;
    private boolean showProgress;
    private String title, message, positiveLabel, negativeLabel, info;
    private int messageTextColor = -1;
    private int infoTextColor = -1;

    public static MessageDialog newInstance(String tag) {
        MessageDialog d = new MessageDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false, false);
        if (savedInstanceState != null) {
            title = savedInstanceState.getString(TAG_TITLE);
            message = savedInstanceState.getString(TAG_MESSAGE);
            info = savedInstanceState.getString(TAG_INFO);
            positiveLabel = savedInstanceState.getString(TAG_POSITIVE_LABEL);
            negativeLabel = savedInstanceState.getString(TAG_NEGATIVE_LABEL);
            showProgress = savedInstanceState.getBoolean(TAG_SHOW_PROGRESS);
        } else {
            if (title == null)
                title = "";
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_message;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        titleText = fv(R.id.text_dlg_title);
        messageText = fv(R.id.text_message);
        infoText = fv(R.id.text_info);
        buttonLayout = fv(R.id.layout_button);
        positiveButton = fv(R.id.button_positive);
        negativeButton = fv(R.id.button_negative);
        bodyLayout = fv(R.id.layout_body);
        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.8f;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            rate = 0.5f;

        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_TITLE, title);
        outState.putString(TAG_MESSAGE, message);
        outState.putString(TAG_INFO, info);
        outState.putString(TAG_POSITIVE_LABEL, positiveLabel);
        outState.putString(TAG_NEGATIVE_LABEL, negativeLabel);
        outState.putBoolean(TAG_SHOW_PROGRESS, showProgress);
    }

    public MessageDialog setTitle(String title) {
        this.title = title;

        if (!isViewCreated())
            return this;

        if (title != null)
            titleText.setText(title);

        titleText.setVisibility(!TextUtils.isEmpty(title) ? View.VISIBLE : View.GONE);
        bodyLayout.setBackgroundResource(!TextUtils.isEmpty(title) ? R.drawable.dialog_body : R.drawable.dialog_background);
        return this;
    }

    public MessageDialog setMessage(String message) {
        this.message = message;

        if (!isViewCreated())
            return this;

        messageText.setText(message != null ? message : "");
        return this;
    }

    public MessageDialog setInfo(String info) {
        this.info = info;

        if (!isViewCreated())
            return this;

        infoText.setText(info != null ? info : "");
        infoText.setVisibility(info != null ? View.VISIBLE : View.GONE);
        return this;
    }

    public MessageDialog setMessageTextColor(int color) {
        this.messageTextColor = color;

        if (!isViewCreated())
            return this;

        messageText.setTextColor(messageTextColor);

        return this;
    }

    public MessageDialog setInfoTextColor(int color) {
        this.infoTextColor = color;

        if (!isViewCreated())
            return this;

        infoText.setTextColor(infoTextColor);

        return this;
    }

    public MessageDialog setPositiveLabel(String positiveLabel) {
        this.positiveLabel = positiveLabel;

        if (!isViewCreated())
            return this;

        if (positiveLabel != null)
            positiveButton.setText(positiveLabel);

        positiveButton.setVisibility(positiveLabel != null ? View.VISIBLE : View.GONE);

        setButtonLayout();
        return this;
    }

    public MessageDialog setNegativeLabel(String negativeLabel) {
        this.negativeLabel = negativeLabel;

        if (!isViewCreated())
            return this;

        if (negativeLabel != null)
            negativeButton.setText(negativeLabel);
        negativeButton.setVisibility(negativeLabel != null ? View.VISIBLE : View.GONE);

        setButtonLayout();
        return this;
    }

    private void initializeViews() {
        positiveButton.setOnClickListener(v -> {
            if (positiveListener != null)
                positiveListener.onDialogPositive(MessageDialog.this, tag);

            dismiss();
        });

        negativeButton.setOnClickListener(v -> {
            if (negativeListener != null)
                negativeListener.onDialogNegative(MessageDialog.this, tag);

            dismiss();
        });

        setTitle(title);
        setMessage(message);
        if (messageTextColor != -1)
            setMessageTextColor(messageTextColor);
        setInfo(info);
        if (infoTextColor != -1)
            setInfoTextColor(infoTextColor);
        setPositiveLabel(positiveLabel);
        setNegativeLabel(negativeLabel);
    }

    private boolean isViewCreated() {
        return negativeButton != null;
    }

    private void setButtonLayout() {
        buttonLayout.setVisibility(positiveLabel == null && negativeLabel == null ? View.GONE : View.VISIBLE);
    }
}
